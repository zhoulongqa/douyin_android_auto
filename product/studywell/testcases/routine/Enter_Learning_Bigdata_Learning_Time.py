# -*- coding: utf-8 -*-
import unittest
from product.studywell.base.BaseTest import BaseTest
from utils.eventutils import EventCheck
from product.studywell.pages.MainPages import MainPage

class Enter_Learning_Bigdata_Leanring_Time(BaseTest):
    def setUp(self):
        """
        埋点事件前置操作写在这里
        """
        self.uihelper.click_element(MainPage['我的'])

    def tearDown(self):
        """
        恢复初始状态操作写在这里，如果没有多个Case，不需要写
        """

    @EventCheck.check_step(event='Enter_Learning_Bigdata_Learning_Time', wait_time=5)
    def test_01(self):
        self.uihelper.click_element(MainPage['学习'])


if __name__ == "__main__":
    unittest.main(verbosity=2)