# -*- coding: utf-8 -*-
from product.toutiao.pages.MainPage import MainPage
from utils.eventutils.EventHttpHandler import *
from product.toutiao.configs.AccountConfig import AccountConfig
import time
import os


func_map = {
    '登录流程': 'login',
    '上报地址修改流程': 'change_ip',
    '测试准备流程': 'prepare',
}


def prepare(self):
    # self.uihelper.popup_window_detect_and_click()
    try:
        self.driver.switch_to.alert.accept()
        CommFun.Logger.INFO('弹窗处理成功')
        CommFun.sleep(2)
        self.driver.switch_to.alert.accept()
        CommFun.Logger.INFO('弹窗处理成功')
    except:
        CommFun.Logger.INFO('无弹窗需要处理')
    CommFun.sleep(1)
    login(self, phone=AccountConfig.account, password=AccountConfig.password)
    port = CommFun.get_available_port()
    ip = CommFun.get_local_ip()
    host = ip + ":" + str(port)
    os.environ['EVENTSERVER'] = host
    process_args = {"args": ["-TTTrackRemoteServerCacheKey", "http://" + host]}
    os.environ['PROCESSARGS'] = str(process_args).replace("'", "\"")
    # host = os.getenv('EVENTSERVER')
    # change_ip(self, host=host)
    EventHttpServer.PORT = port
    EventHttpServer.start_server(ip)


def login(self, phone, password):
    """
    登录流程
    :param self:
    :param phone: 电话号
    :param password: 密码
    :return: Bool
    """
    if self.uihelper.exist(MainPage['我的'], timeout=1):
        self.uihelper.click_element(MainPage['我的'])
    else:
        self.uihelper.click_element(MainPage['底部未登录'])
    login_flag = self.uihelper.find_element(MainPage['已登录账号'],time=1)
    if login_flag:
        self.uihelper.click_element(MainPage['系统设置'])
        self.uihelper.swipe_up()
        self.uihelper.click_element(MainPage['退出登录'])
        self.uihelper.click_element(MainPage['确认退出'])
        self.uihelper.click_element(MainPage['电话登录'])
        if not self.uihelper.exist(MainPage['账号密码登录文字'], timeout=1):
            Logger.INFO("----默认为验证码登录----")
            self.uihelper.click_element(MainPage['账号密码登录'])
            if not self.uihelper.exist(MainPage['账号密码登录文字'], timeout=1):
                self.uihelper.click_element(MainPage['账号密码登录'])
        else:
            Logger.INFO("----默认为密码的登录----")
        self.uihelper.enter(MainPage['手机号输入框'], phone)
        self.uihelper.enter(MainPage['密码输入框'], password)
        self.uihelper.click_element(MainPage['进入头条'])
        time.sleep(1)
    else:
        self.uihelper.click_element(MainPage['电话登录'])
        if not self.uihelper.exist(MainPage['账号密码登录文字'], timeout=1):
            Logger.INFO("----默认为验证码登录----")
            self.uihelper.click_element(MainPage['账号密码登录'])
            if not self.uihelper.exist(MainPage['账号密码登录文字'], timeout=1):
                self.uihelper.click_element(MainPage['账号密码登录'])
        else:
            Logger.INFO("----默认为密码的登录----")
        self.uihelper.enter(MainPage['手机号输入框'], phone)
        self.uihelper.enter(MainPage['密码输入框'], password)
        self.uihelper.click_element(MainPage['进入头条'])
        time.sleep(2)
    return True


def change_ip(self, max_scroll_times=5, host=''):
    """
    修改日志上报地址流程
    :param self:
    :param max_scroll_times: 最大滑动次数
    :param host: 上报地址 e.g.:  10.2.193.190:10303
    :return: Bool
    """
    self.uihelper.click_element(MainPage['系统设置'])
    self.uihelper.click_element(MainPage['高级调试'])
    self.uihelper.swipe_up(max_scroll_times)
    self.uihelper.click_element_coordinate(MainPage['地址输入框'])
    self.uihelper.sleep(1)
    self.uihelper.enter(MainPage['地址输入框'],host)
    self.uihelper.click_element(MainPage['Done'])
    self.uihelper.click_element(MainPage['确定'])
    self.uihelper.click_element(MainPage['关闭'])
    return self.uihelper.click_element(MainPage['登录返回'])
