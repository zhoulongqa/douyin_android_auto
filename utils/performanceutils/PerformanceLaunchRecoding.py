# -*- coding: utf-8 -*-
from os import path
import sys
basedir = path.realpath(path.join(path.dirname(__file__), '../..'))
sys.path.append(basedir)
reload(sys)
import multiprocessing
import subprocess
import time
import os
import wda
import re
import biplist
from utils import Logger


def get_app_info(app_url):
    cmd_download_target_app = "curl -o NewsInHouse.ipa {}".format(app_url)
    cmd_unzip_ipa = "unzip -o NewsInHouse.ipa"
    os.system(cmd_download_target_app)
    os.system(cmd_unzip_ipa)
    file_path = os.listdir("Payload")[0]
    info = biplist.readPlist("Payload/"+file_path+"/Info.plist")
    bundle_name = info["CFBundleName"]
    bundle_id = info["CFBundleIdentifier"]
    version = info["CFBundleShortVersionString"]
    uvc = info["CFBundleVersion"].replace('.', '')
    return bundle_id,version,uvc


def record_screen():
    local_path = os.path.abspath('.')
    filepath = re.findall(r'(.*)performanceutils', local_path, re.M)[0]
    subprocess.call('osascript '+filepath+'performanceutils/recordingScreen.scpt', shell=True)


def open_quick_time():
    local_path = os.path.abspath('.')
    filepath = re.findall(r'(.*)performanceutils', local_path, re.M)[0]
    subprocess.call('osascript ' + filepath + 'performanceutils/openQuickTime.scpt', shell=True)


def start_recording():
    local_path = os.path.abspath('.')
    filepath = re.findall(r'(.*)performanceutils', local_path, re.M)[0]
    subprocess.call('osascript ' + filepath + 'performanceutils/startRecordingScreen.scpt', shell=True)


def start_wda(wda_port="10000", udid=""):
    os.chdir('WebDriverAgent')
    os.system(
        "USE_PORT=%s xcodebuild -project WebDriverAgent.xcodeproj \
       -scheme WebDriverAgentRunner \
       -destination 'id=%s' test" % (wda_port, udid))


def start_iproxy_server(local_port, wda_port, udid):
    os.system("iproxy %s %s %s" % (local_port, wda_port, udid))


def run_test(app_url, udid, task, wda_port='10001', local_port='10001',
             movie_path='/Users/bytedance/Movies/ScreenRecording', member='guohao.qa'):
    try:
        os.system('rm -f NewsInHouse.ipa')
        os.system('rm -rf Payload')
    except:
        print "No file to remove"
    try:
        os.system('rm -rf '+movie_path+'/*.qtpxcomposition')
        os.system('rm -rf '+movie_path+'/*.mp4')
    except:
        print "No file to remove"
    # 安装App
    version = ''
    app_url = app_url.replace(' ', '')
    if app_url.startswith("http"):
        info = get_app_info(app_url)
        bundle_id = info[0]
        Logger.INFO('app bundle id:' + bundle_id)
        version = info[1]
        uvc = info[2]
        Logger.INFO('App version:' + version)
        os.system('ideviceinstaller -u ' + udid + ' -i NewsInHouse.ipa')
    else:
        bundle_id = app_url
        uvc = '0'
        Logger.INFO('app bundle id:' + bundle_id)
        app_info = subprocess.Popen("ideviceinstaller -l -u "+udid, stdout=subprocess.PIPE, shell=True)
        for l in app_info.stdout.readlines():
            lstr = l.decode('utf-8')
            if lstr.startswith(bundle_id):
                version = lstr.split(' ')[3].replace('\n', '')
                Logger.INFO('App version:'+version)
                break

    # 准备WDA
    os.system(
        'cp -R /usr/local/lib/node_modules/appium/node_modules/appium-xcuitest-driver/WebDriverAgent WebDriverAgent')
    Logger.INFO(
        '-----------------------start recording--------------------')
    record_thread = multiprocessing.Process(target=open_quick_time)
    record_thread.daemon = True
    record_thread.start()
    Logger.INFO(
        '-----------------------waiting for record_thread--------------------')
    record_thread.join()
    time.sleep(5)
    os.system("ideviceinstaller -u "+udid+" -U com.bytedance.WebDriverAgent")
    Logger.INFO(
        '-----------------------start wda server-----%s--------%s-------' % (wda_port, udid))
    wda_thread = multiprocessing.Process(target=start_wda, args=(wda_port, udid))
    wda_thread.daemon = True
    wda_thread.start()
    time.sleep(10)
    Logger.INFO(
        '-----------------------start iproxy server-----%s--------%s-------' % (wda_port, udid))
    iproxy_thread = multiprocessing.Process(target=start_iproxy_server, args=(local_port, wda_port, udid))
    iproxy_thread.daemon = True
    iproxy_thread.start()
    time.sleep(4)
    driver = wda.Client("http://localhost:"+local_port)
    record_thread = multiprocessing.Process(target=start_recording)
    record_thread.daemon = True
    record_thread.start()
    Logger.INFO(
        '-----------------------waiting for record_thread--------------------')
    time.sleep(5)
    Logger.INFO(
        '-----------------------launch app-------------%s-------' % bundle_id)
    app_session = driver.session(bundle_id)
    # if bundle_id == 'com.sina.weibo':
    #     app_session.tap(360,610)
    # elif bundle_id == 'com.tencent.reading':
    #     app_session.tap(360, 530)
    # elif bundle_id == 'com.tencent.info':
    #     app_session.tap(360, 530)
    try:
        if app_session.alert.wait(5):
            app_session.alert.accept()
    except:
        Logger.INFO('-----handle alert Failed-------')
    time.sleep(30)
    # app_session.close()
    os.chdir(movie_path)
    time.sleep(5)
    os.system('mv -f *.qtpxcomposition/*.mov ./test.mp4')
    os.system('rm -rf *.qtpxcomposition')
    print "handle mov"
    iproxy_thread.terminate()
    wda_thread.terminate()

    curl_cmd = 'curl -X POST "https://ci.bytedance.net/job/TT_Video_Analysis_Performance/buildWithParameters?'\
               'APP_VERSION='+version+'&PACKAGE_NAME='+bundle_id+'&UDID=49544329298&IS_NEXUS=False&'\
               'CID=0&UPDATE_VERSION_CODE='+uvc+'&TASK_ID='+task+'&TASK_INDEX=0&DEVICE_OS=11.0.2' \
               '&OS=1&MEMBER='+member+'" -F "demo.mp4=@./test.mp4"'
    Logger.INFO('curl command'+curl_cmd)
    os.system(curl_cmd)


if __name__ == "__main__":
    import sys
    from optparse import OptionParser

    parser = OptionParser()
    parser.add_option("-u", "--URL",
                      help="ipa url")
    parser.add_option("-d", "--device",
                      help="device udid")
    parser.add_option("-p", "--path",
                      help="movie path")
    parser.add_option("-t", "--task",
                      help="task id")
    parser.add_option("-m", "--member",
                      help="member id")
    options, args = parser.parse_args(sys.argv)
    url = options.URL
    device = options.device
    movie_path = options.path
    task = options.task
    member = options.member
    run_test(url, device, movie_path=movie_path, task=task, member=member)
