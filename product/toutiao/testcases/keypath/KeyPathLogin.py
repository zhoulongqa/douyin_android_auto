# -*- coding: utf-8 -*-
import unittest
from product.toutiao.base.BaseTest import BaseTest
from product.toutiao.testcases.commonprocess import PublicProcess
from product.toutiao.configs.AccountConfig import AccountConfig


class KeyPathLogin(BaseTest):
    def setUp(self):
        """
        前置操作写在这里
        """

    def tearDown(self):
        """
        恢复初始状态操作写在这里，如果没有多个Case，不需要写
        """
        self.uihelper.sleep(1)

    def test_01(self):
        PublicProcess.login(self, phone=AccountConfig.account, password=AccountConfig.password)


if __name__ == "__main__":
    unittest.main(verbosity=2)