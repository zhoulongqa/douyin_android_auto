# -*- coding: utf-8 -*-
import json
import requests
import time
import os


def upload_case_info(pid=0, cid=0, device_id='34648987183', aid=13, platform=1, value_type=1, task_id=1, task_index=1,
                     spid=-1,
                     start_time='2018-04-11T18:45:02+08:00', end_time='2018-04-18T18:45:02+08:00'):
    """
    字段含义规则见https://wiki.bytedance.net/pages/viewpage.action?pageId=187264805
    """
    url = 'http://10.8.78.136:9317/notifyperformancedatas'
    headers = {'content-type': 'application/json'}
    content = {'pid': pid,
               'cid': cid,
               'spid': spid,
               'device_id': device_id,
               'aid': aid,
               'os': platform,
               'value_type': value_type,
               'taskId': task_id,
               'taskIndex': task_index,
               'start_time': start_time,
               'end_time': end_time,
               }
    env_task = os.getenv('TASKID')
    if env_task:
        content['taskId'] = env_task

    data = requests.post(url, data=json.dumps(content), headers=headers)
    # data = type(data)
    print data


def get_format_time():
    return time.strftime('%Y-%m-%dT%H:%M:%S+08:00', time.localtime())





if __name__ == '__main__':
    # upload_case_info()
    print get_format_time()