# -*- coding: UTF-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from product.toutiao.pages import package_name


MainPage = {
    # 首页导航栏
    '我的': '(mineIconButton)',
    '底部未登录': ('未登录','TEXT'),
    '搜索框': package_name + ':id/search_bar_search_content',
    '发布': package_name + ':id/new_search_bar_mediamaker_layout',

    # 频道
    '频道条': package_name + ':id/category_strip',
    '更多频道': package_name + ':id/icon_category',
    '关注': ('关注', 'ACCESSIBILITY_ID'),
    '推荐': ('推荐', 'ACCESSIBILITY_ID'),
    '电影': ('电影', 'ACCESSIBILITY_ID'),
    '热点': ('热点', 'ACCESSIBILITY_ID'),
    '问答': ('问答', 'ACCESSIBILITY_ID'),
    '视频': ('视频', 'ACCESSIBILITY_ID'),

    # Feed流
    'Feed流列表': package_name + ':id/root',
    '刷新按钮': package_name + ':id/refresh',

    # 西瓜视频列表页
    '视频列表': package_name + ':id/root',
    '视频作者': package_name + ':id/video_detail_user_name',
    '列表播放视频': '(playButton)',
    '相关视频': '(TTVDetailRelatedVideoCell)',
    '评论': '(enable)',
    '写评论': '写评论...',
    '视频评论输入框': '(internalTextView)',


    # 文章/视频/图集详情页评论区
    '详情页-写评价': package_name + ':id/write_comment_layout',
    '详情页-查看评论': package_name + ':id/view_comment_layout',
    '详情页-点击收藏': package_name + ':id/action_favor',
    '详情页-点击分享': package_name + ':id/action_repost',


    # 底Tab
    '首页': ('首页', 'TEXT'),
    '西瓜视频': ('西瓜视频','TEXT'),
    '微头条': ('微头条', 'TEXT'),
    '小视频': ('小视频', 'TEXT'),


    #搜索中间页
    '搜索中间页返回': package_name + ':id/search_cancel',
    '搜索中间页输入框': package_name + ':id/search_input',
    '搜索中间页搜索': package_name + ':id/right_btn_search',
    '搜索热词左一词': package_name + ':id/search_header_left_word',
    '搜索热词中间词': package_name + ':id/search_header_mid_word',
    '搜索热词右一词': package_name + ':id/search_header_right_word',

    
    #我的页面
    '消息通知': ('消息通知', 'TEXT')



}

