# -*- coding: UTF-8 -*-
import BaseHTTPServer
import json
import threading
import thread
import urllib
import urlparse
import Queue
import sys
from os import path
basedir = path.realpath(path.join(path.dirname(__file__), '../../..'))
sys.path.append(basedir)
reload(sys)

from BaseHTTPServer import BaseHTTPRequestHandler
from SocketServer import BaseRequestHandler, ThreadingMixIn
from utils import  Logger
from ThreadPoolMixIn import ThreadPoolMixIn
from utils import CommFun

lock = thread.allocate_lock()

event_queue = Queue.Queue(maxsize=50)
event_arr = []

# 此参数方便存储时间，applog1.0 key 为label，applog3.0 key 为 event
event_key = "tag"
def modify_event_key(value):
    global event_key
    event_key = value


class EventHttpHandler(BaseHTTPRequestHandler):
    received_events = {}
    switch_state = False
    flag = False

    @classmethod
    def start_observer(cls):
        #以阻塞的形式设置标志位，如果当前还在接受log，则等当前log接受完再设置标志位
        #防止之前的log影响待验证事件的接受
        if EventHttpHandler.switch_state is True:
            return

        while EventHttpHandler.flag is not True:
            EventHttpHandler.flag = True
            Logger.INFO("wait for log receive finished on start observer!!!")
            CommFun.sleep(2)

        EventHttpHandler.switch_state = True

    @classmethod
    def stop_observer(cls):
        if EventHttpHandler.switch_state is False:
            return
        while EventHttpHandler.flag is not True:
            EventHttpHandler.flag = True
            Logger.INFO("wait fro log receive finished on stop observer!!!")
            CommFun.sleep(2)
        EventHttpHandler.switch_state = False

    def handle(self):
        BaseHTTPRequestHandler.handle(self)
        self.finish()

    def do_GET(self):
        """Android is GET"""
        EventHttpHandler.flag = False
        print '-----------------get start---------------------',threading.currentThread().getName()
        self.protocal_version = 'HTTP / 1.1'
        try:
            query = urllib.unquote(self.path).decode("utf-8", 'ignore')
            para_dict =  urlparse.parse_qs(self.path,True)
            event = para_dict['/?parameter'][0]

        except IndexError:
            self.send_response(200, 'OK')
            self.send_header("EventTest", "no data")
            self.end_headers()
            self.finish()
            return
        if not EventHttpHandler.switch_state:
            try:
                event = json.loads(event)
            except Exception as e:
                Logger.INFO("收到埋点格式错误导致无法解析，对应埋点如下" + str(event))
                self.send_response(200, 'OK')
                self.send_header("EventTest","no data")
                self.end_headers()
                self.finish()
                return
            """适配Applog3.0 可能有问题，改为event.has_key('impression')吧"""
            if 'event' in event:
                key = event['event']
                # print "---------------------impression------------------%s"%key
                # print event
                # print "---------------------impression------------------%s"%key

        if EventHttpHandler.switch_state:
            print '----------------------------------------------------'
            Logger.INFO(event)
            print '----------------------------------------------------'

        self.__add_event(event)
        self.send_response(200,'OK')
        self.send_header("EventTest", "Contect")
        self.end_headers()
        self.wfile.write(event)
        self.finish()


    def do_POST(self):
        """iOS is post"""
        EventHttpHandler.flag = False
        # print '-----------------post start---------------------', threading.currentThread().getName()
        # Logger.INFO(self.__dict__)
        para = urllib.unquote(self.rfile.read(int(self.headers['content-length']))).decode("utf-8", 'ignore')
        event = para[para.index('=')+1:]
        # print EventHttpHandler.switch_state,event
        if EventHttpHandler.switch_state:
            Logger.INFO("----------receive-------->"+event)
        try:
            self.__add_event(event)
            # if event_queue.qsize() > 40:
            #     print event_queue.get()
            # event_queue.put(event)
            # event_arr.append(event)
            # print 'event arr len',len(event_arr)
            # Logger.INFO("----------event_queue-------->" + str(event_queue.qsize()))
        except Exception,e:
            Logger.ERROR(e.message)
            pass
        self.send_response(200, 'OK')
        self.send_header("EventTest", "Contect")
        self.end_headers()
        self.wfile.write(event)
        self.finish()
        pass

    def log_message(self, format, *args):
        return

    def __get_event_key(self, event):
        """for every event to be added, verify if it is 1.0 or 3.0"""
        if 'label' in event:
            # event 1.0
            event_key = 'label'
        else:
            # event 3.0
            event_key = 'event'
        return event_key

    def __add_event(self,event):

        # lock.acquire()
        if EventHttpHandler.switch_state:
            obj={}
            temp=[]
            try:
                obj = json.loads(event)
                event_key = self.__get_event_key(obj)
                print '+++++ event key is {} +++++'.format(event_key)
                key = obj[event_key]
            except KeyError:
                print '---------screen---------------'
                key = 'screen'
            except ValueError:
                print '---------value error----------'
                return
            except TypeError:
                print '---------type error-----------'
                return
            try:
                temp = EventHttpHandler.received_events[key]
            except KeyError:
                temp = []
            finally:
                print '+-+-+-+-+ key is {} +-+-+-+-+'.format(key)
                temp.append(obj)
                EventHttpHandler.received_events[key] = temp
                Logger.INFO("----------add-------->" + event)
                Logger.INFO("<--------received_events for %s event---------->"%key + str(len(temp)))
        # lock.release()

    @staticmethod
    def clear_events():
        EventHttpHandler.received_events.clear()


class EventHttpServer(ThreadPoolMixIn, BaseHTTPServer.HTTPServer):
# class EventHttpServer(BaseHTTPServer.HTTPServer):
    PORT = 10303

    @staticmethod
    def start_server(host):
        Logger.INFO('----------------start_srver----------------------' + host+":"+str(EventHttpServer.PORT))
        server = EventHttpServer((host, EventHttpServer.PORT), EventHttpHandler)
        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.setDaemon(True)
        server_thread.start()


if __name__ == '__main__':
    EventHttpServer.PORT = 10308
    # EventHttpHandler.switch_state = True
    EventHttpServer.start_server('10.2.205.171')
    print "Server loop running in thread^^"
    while True:
        pass
