# -*- coding: utf-8 -*-
from product.toutiao.base.BaseTest import BaseTest
import unittest
from utils.eventutils import EventCheck
from product.toutiao.pages.MainPage import MainPage


class Event_New_Tab_Enter_Flip(BaseTest):
    def setUp(self):
        """
        埋点事件前置操作写在这里
        :return:
        """
        self.uihelper.click_element(MainPage['电影'])
        self.uihelper.sleep(1)

    def tearDown(self):
        """
        恢复初始状态操作写在这里，如果没有多个Case，不需要写
        :return:
        """

    @EventCheck.check_step(event='Event_New_Tab_Enter_Flip', wait_time=5)
    def test_01(self):
        self.uihelper.swipe_right()


if __name__ == '__main__':
    unittest.main(verbosity=2)
