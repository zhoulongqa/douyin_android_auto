# -*- coding: UTF-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from yaml import load
from utils import Logger
import importlib
import os


def open_YAML(file_path):
    with open(file_path,'rb') as f:
        cont = f.read()
        cf = load(cont)
        return cf


def run_actions(self, actions, page):
    for key, value in actions.items():
        key = key.encode('utf-8')
        if isinstance(value, unicode):
            value = value.encode('utf-8')
            getattr(self.uihelper, self.uihelper.func_map[key])(page[value])
        elif isinstance(value, int):
            getattr(self.uihelper, self.uihelper.func_map[key])(value)
        elif isinstance(value, list):
            if len(value) == 1:
                text = page[value[0].encode('utf-8')]
                expr = getattr(self.uihelper, self.uihelper.func_map['查找控件'])(text)
                getattr(self.uihelper, self.uihelper.func_map[key])(expr)
            else:
                text1 = page[value[0].encode('utf-8')]
                text2 = value[1].encode('utf-8')
                text3 = value[2].encode('utf-8')
                expr = getattr(self.uihelper, self.uihelper.func_map[text2])(text1)
                getattr(self.uihelper, self.uihelper.func_map[key])(expr, text3)
        elif isinstance(value, dict):
            if 'text' in value:
                text = page[value['text'].encode('utf-8')]
                args_str = ','.join(value['args'])
                fun_str = 'self.uihelper.' + self.uihelper.func_map[key] + '(\'' + text + '\',' + args_str + ')'
                Logger.INFO(fun_str)
                eval(fun_str)
            elif 'file' in value:
                file_name = value['file']
                product = os.getenv('PRODUCT')
                process = importlib.import_module('product.'+product+'.testcases.commonprocess.'+file_name)
                if 'args' in value:
                    args_str = ','.join(value['args'])
                    func_name = eval("process.func_map['" + key + "']")
                    fun_str = 'process.' + func_name + '(self,' + args_str + ')'
                else:
                    func_name = eval("process.func_map['" + key + "']")
                    fun_str = 'process.' + func_name + '(self)'
                Logger.INFO(fun_str)
                eval(fun_str)
            elif 'route' in value:
                url_name = value['route'].encode('utf-8')
                product = os.getenv('PRODUCT')
                router = importlib.import_module('product.'+product+'.route.Router')
                url = eval("router.Router.route_map['" + url_name + "']")
                getattr(self.uihelper, self.uihelper.func_map[key])(url)
            else:
                if 'args' in value:
                    for num, item in enumerate(value['args']):
                        if isinstance(item, unicode):
                            value['args'][num] = "\""+item+"\""
                    args_str = ','.join(value['args'])
                    fun_str = 'self.uihelper.' + self.uihelper.func_map[key] + '(' + args_str + ')'
                else:
                    fun_str = 'self.uihelper.' + self.uihelper.func_map[key] + '(self)'
                Logger.INFO(fun_str)
                eval(fun_str)
        elif not value:
            fun_str = 'self.uihelper.' + self.uihelper.func_map[key] + '()'
            Logger.INFO(fun_str)
            eval(fun_str)



if __name__ == '__main__':
    case = open_YAML(file_path='EventDemo.yaml')
    for i in case['SetUp']:
        for k,v in i.items():
            print type(v)
            # for pn, pv in v.items():
            #     if isinstance(pv,str):
            #         v[pn] = pv.encode('utf-8')
