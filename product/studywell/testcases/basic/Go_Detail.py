# -*- coding: utf-8 -*-
#核心埋点--进入详情页面
import unittest
from product.studywell.base.BaseTest import BaseTest
from utils.eventutils import EventCheck
from product.studywell.pages.MainPages import MainPage
class Go_Detail(BaseTest):
    def setUp(self):
        """
        埋点事件前置操作写在这里
        """
        self.uihelper.swipe_up(1)
        self.uihelper.sleep(2)
        # a=self.driver.page_source.find(MainPage['好书推荐One'])
        # self.driver.find_element_by_xpath('//XCUIElementTypeStaticText[@name="好书推荐"]/../XCUIElementTypeCollectionView/XCUIElementTypeCell/').click()
    def tearDown(self):
        """
        恢复初始状态操作写在这里，如果没有多个Case，不需要写
        """

    @EventCheck.check_step(event='Go_Detail', wait_time=5)
    def test_01(self):
        self.uihelper.click_element(MainPage['读好书'])


if __name__ == "__main__":
    unittest.main(verbosity=2)