# -*- coding: utf-8 -*-
from product.douyin.pages.MainPage import MainPage
import time
from utils import CommFun


func_map = {
    '登录流程': 'login',
    '上报地址修改流程': 'change_ip',
}


def is_main_page(self):
    """
    判断是否为首页
    :param self:
    :return:  Bool
    """
    result = self.uihelper.find_element(MainPage['Feed流列表'], time=12)
    print '----------hello--------------'
    if not result:
        print 'False'
        return False
    else:
        print 'True'
        return True


def login(self, phone, password):
    """
    登录流程
    :param self:
    :param phone: 电话号
    :param password: 密码
    :return: Bool
    """
    # 账号密码登录
    CommFun.sleep(5)
    self.uihelper.click_element(MainPage['我'])
    try:
        self.uihelper.click_element(MainPage['密码登录按钮'])
    except:
        self.uihelper.click_element(MainPage['首页'])
    else:
        self.uihelper.enter(MainPage['手机号输入框'], phone)
        self.uihelper.enter(MainPage['密码输入框'], password)
        self.driver.hide_keyboard()
        self.uihelper.click_element(MainPage['确认按钮'])
    return True


def change_ip(self):
    """
    修改日志上报地址流程
    :param self:
    :param max_scroll_times: 最大滑动次数
    :param host: 上报地址 e.g.:  10.2.193.190:10303
    :return: Bool
    """
    # 连接日志服务
    self.uihelper.click_screen_coordinate(200,200)
    CommFun.sleep(5)
    self.uihelper.click_element(MainPage['我'])
    self.uihelper.click_element(MainPage['更多按钮'])
    self.uihelper.click_element(MainPage['设置'])
    self.uihelper.swipe_up()
    self.uihelper.click_element(MainPage['测试按钮'])
    self.uihelper.enter(MainPage['日志服务器地址'], '10.1.61.84:10303')
    self.uihelper.click_element(MainPage['确定'])
    self.uihelper.click_element(MainPage['返回按钮'])
    self.uihelper.click_element(MainPage['测试按钮'])
    self.uihelper.click_element(MainPage['确定'])
    self.uihelper.click_element(MainPage['返回按钮'])
    self.uihelper.click_element(MainPage['返回按钮'])
    self.uihelper.click_element(MainPage['首页'])

#取消升级弹窗
def update_cancel(self):
    while True:
        self.uihelper.swipe_up()
        if self.uihelper.find_element(MainPage['以后再说']):
            self.uihelper.click_element(MainPage['以后再说'])
            break
    print "update cancel"

#安装app
def install_app(self,package,package_url):
    print u"开始安装"
    if self.driver.is_app_installed(package):
        return True
    else:
        return self.driver.install_app(package_url)

#卸载app
def uninstall_app(self,package):
    print "开始卸载"
    if self.driver.is_app_installed(package):
        self.driver.remove_app(package)

