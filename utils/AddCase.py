# -*- coding: UTF-8 -*-
import os
import re


template = '__all__ = []'


def add_case(root_dir):
    files_list = os.listdir(root_dir)
    tmp = []
    for i in files_list:
        if i == '__init__.py':
            continue
        else:
            i = i.replace('.py', '')
            tmp.append(i)
    files_str = '__all__ = '+str(tmp).replace(',', ', \n   ').replace('[', '[\n    ').replace(']', '\n]\n')

    with open(root_dir+'/__init__.py', 'w+') as f:
        f.write(files_str)


def add_case_yml(root_dir):
    file_list = os.listdir(root_dir)
    tmp = []
    for i in file_list:
        if i.endswith(".yml"):
            i = i.replace(".yml", '')
            tmp.append(i)
    print str(tmp).replace('\'', '').replace(' ', '')


if __name__ == '__main__':
    # add_case('/Users/guohao/UIAutoTest/product/toutiao/testcases/ttperformance')
    add_case_yml('/Users/guohao/UIAutoTest/product/toutiao/testcases/yamlcase')