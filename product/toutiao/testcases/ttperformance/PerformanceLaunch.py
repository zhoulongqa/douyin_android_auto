# -*- coding: utf-8 -*-
import unittest
from product.toutiao.base.BaseTest import BaseTest
from utils.performanceutils import UploadCaseInfo


class PerformanceLaunch(BaseTest):
    def setUp(self):
        """
        前置操作写在这里
        """
        self.driver.close_app()

    def tearDown(self):
        """
        恢复初始状态操作写在这里，如果没有多个Case，不需要写
        """
        self.uihelper.sleep(1)

    def test_01(self):
        for i in xrange(10):
            start = UploadCaseInfo.get_format_time()
            self.driver.launch_app()
            self.uihelper.sleep(3)
            self.driver.close_app()
            end = UploadCaseInfo.get_format_time()
            self.uihelper.sleep(1)
            UploadCaseInfo.upload_case_info(pid=0, cid=0, value_type=0, task_id='', task_index=i,
                                            start_time=start, end_time=end)


if __name__ == "__main__":
    unittest.main(verbosity=2)