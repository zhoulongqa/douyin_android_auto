# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from product.studywell.base.BaseTest import BaseTest
import unittest
from utils.eventutils import EventCheck
from product.studywell.pages.MainPages import MainPage
from utils import YAMLHelper
from utils.CommFun import Logger
import os
from os import path
import re
from product.studywell.testcases.commonprocess import PublicProcess


local_path = path.abspath('.')
if 'testcases' in local_path:
    filepath = re.findall(r'(.*)testcases', local_path, re.M)[0]
else:
    evn_product = os.getenv('PRODUCT')

    filepath = local_path + '/product/' + evn_product

case = YAMLHelper.open_YAML(filepath+'/testcases/yamlcase/prepare.yml')
if 'TeaEvent' in case:
    tea_event = case['TeaEvent']
else:
    tea_event = 0
if 'Schema' in case:
    local_event = case['Schema']
else:
    local_event = 0


class EventDemo(BaseTest):
    def setUp(self):
        actions = case['SetUp']
        if actions is not None:
            for action in actions:
                YAMLHelper.run_actions(self, action, MainPage)
                self.uihelper.sleep(1)
        self.uihelper.sleep(1)

    @EventCheck.check_step(event=local_event, wait_time=case['WaitTime'], tea_event=tea_event)
    def test_01(self):
        actions = case['Event']
        if actions is not None:
            for action in actions:
                YAMLHelper.run_actions(self, action, MainPage)
                self.uihelper.sleep(1)
        self.uihelper.sleep(1)

    def tearDown(self):
        pass


if __name__ == '__main__':
    unittest.main(verbosity=2)
