# -*- coding: UTF-8 -*-
from os import path
import sys
basedir = path.realpath(path.join(path.dirname(__file__), '../../..'))
sys.path.append(basedir)
reload(sys)
from EventMessage import EventMessage
from utils import Logger
from jsonschema import validate
from jsonschema.exceptions import ValidationError
from utils.eventutils import EventHttpHandler


class Verifier(object):
    def verify(self,expect_event={}, received_events={}):
        """
        验证json数据， 无论expect_event为 1 or 3，都兼容
        :param expect_event:
        :param received_events:
        :param flag_verify: 0时使用本地验证,1使用jsonschema验证
        :return:
        """
        return self.verify_by_jsonschema(event_schema=expect_event,received_events=received_events)


    def verify_impression(self,pack_new,pack_old):
        result = {}
        if pack_new == None or pack_old == None:
            result['success'] = False
            result['msg'] = "pack_new或pack_old参数为空"
            return result


    def _get_event_key(self, schema):
        """for every event_schema  verify if it is 1.0 or 3.0"""
        print schema
        if 'label' in schema['properties']:
            # event 1.0
            event_key = 'label'
        else:
            # event 3.0
            event_key = 'event'
        return event_key

    def verify_by_jsonschema(self, event_schema={}, received_events={}):
        event_key = self._get_event_key(event_schema)
        try:
            received = received_events[event_schema['properties'][event_key]['enum'][0]]
        except KeyError:
            return self.__generate_result(-1, event_schema, received_events)

        match_times = 0
        exception = []
        for event in received:
            try:
                validate(event, event_schema)
                match_times = match_times + 1
            except ValidationError as e:
                Logger.ERROR(e)
                exception.append(e.message)
            pass
        return self.__generate_result(match_times, event_schema, received,exception=exception)

    def __generate_result(self,match_times, expect_event, received_events,exception=None):
        if match_times == 1:
            msg = EventMessage.MSG[EventMessage.MSG_EVENT_SUCCESS]
            pass
        elif match_times > 1:
            msg = EventMessage.MSG[EventMessage.MSG_EVENT_SEND_TWICE]
            pass
        elif match_times < 0:
            msg = EventMessage.MSG[EventMessage.MSG_EVENT_NOT_EXIST]
        else:
            msg = EventMessage.MSG[EventMessage.MSG_EVENT_WRONG_PARA]
            pass

        result ={}


        result['success'] = (match_times == 1)
        result['msg'] = msg
        result['expect_event'] = expect_event
        result['received_events'] = received_events
        if exception is not None and len(exception) > 0:
            result['exception'] = exception
        return result

    def macth_item(self,expect_event={}, event={}):
        match_times=0
        if len(set(expect_event.keys()) & set(event.keys())) != len(expect_event.keys()):
            return 0

        if len(set(expect_event.items()) & set(event.items())) == len(expect_event.items()):
            match_times = match_times + 1
        else:
            sensitiveValues = (set(expect_event.items()) ^ set(event.items())) & set(expect_event.items())
            match_result = True
            print sensitiveValues
            # 增加单个元匹配
            for value in sensitiveValues:
                if str(value[1]).find('flag_match') < 0:
                    match_times = 0
                    match_result = (value[1] == event[value[0]])
                    break
                else:
                    # 匹配过程,只要出现not match,就返回0
                    match_times = 0
                    if self.match(value[1], event[value[0]]) == False:
                        match_result = False
                        break
            if match_result:
                match_times = match_times + 1
        return match_times

    FLAG_MATCH_NUMBER = 'flag_match_number'
    FLAG_MATCH_STRING = 'flag_match_string'
    FLAG_MATCH_HTTP = 'flag_match_http'

    def match(self,type='',received_value=''):
        if type == self.FLAG_MATCH_NUMBER:
            return str(received_value).isdigit()
        elif type == self.FLAG_MATCH_STRING:
            return True
        elif type == self.FLAG_MATCH_HTTP:
            return received_value.index('http://') == 0
        return False

    def verify_multi_event_without_count(self, expect_events, received_events):
        result = {}
        i = 0
        success = True
        if isinstance(expect_events, list):
            """multi event"""
            for event_schema in expect_events:
                event_result = self.verify(event_schema, received_events)
                result['event' + str(i)] = event_result
                success = success and event_result['success']
                i = i + 1
            result['success'] = success
            return result
        elif isinstance(expect_events, dict):
            """single event"""
            return self.verify(expect_events, received_events)
        else:
            return {'success':'False','info':'invalid type for expect_events'}


    def verify_multievent(self,expect_event=[], received_events={},flag_verify=0):
        allresult={}
        result1={}
        i = 0
        ad_id =expect_event[0]['expect_event']['properties']['value']['enum'][0]
        # print event_schema['properties']
        print 'ad_id: '
        print ad_id
        expectlength = len(expect_event)
        print '预期长度'+str(expectlength)
        Logger.INFO('预期长度'+str(expectlength))
        print expectlength
        reciveidlen=0
        for key , value in received_events.items():
            for i in range(len(value)):
                if 'value' not in value[i]:
                    break
                # print value[i]
                if str(value[i]['value']) == str(ad_id):
                    reciveidlen = reciveidlen+1
        print 'reciveidlen='+ str(reciveidlen)
        Logger.INFO('recicedlen : '+str(reciveidlen))
        print reciveidlen
        if reciveidlen != expectlength:
            result1['success'] = 'False'
            result1['msg'] = EventMessage.MSG[EventMessage.MSG_EVENT_MORE_THAN_EXPECTED_EVENT]
            result1['expect_event'] = expect_event
            result1['received_events'] = received_events
            return result1
        for expectevent in expect_event:
            print 'expect_event '+ str(expectevent)
            result = Verifier().verify(expect_event=expectevent['expect_event'], received_events=received_events)
            if result['success'] is not True:
                print '第 '+str(i) +' 个 失败, 预期是:' +str(expectevent)
                Logger.INFO('第 '+str(i) +' 个 失败, 预期是:' +str(expectevent))
                print expect_event
                print received_events

                return result
            allresult[str(i)]=result
            allresult['success'] = 'True'
            allresult['msg'] = EventMessage.MSG[EventMessage.MSG_EVENT_SUCCESS]
            allresult['expect_event'] = expect_event
            allresult['received_events'] = received_events
            i=i+1
        print str(i) + '个埋点校验都成功了'
        Logger.INFO(str(i) + '个埋点校验都成功了')
        return allresult

    def verify_tea_event_multi(self, expect_events, received_events):
        result = {}
        i = 0
        success = True
        if isinstance(expect_events, list):
            """multi event"""
            for event_schema in expect_events:
                event_result = self.verify_tea(event_schema, received_events)
                result['event' + str(i)] = event_result
                success = success and event_result['success']
                i = i + 1
            result['success'] = success
            return result
        elif isinstance(expect_events, dict):
            """single event"""
            return self.verify_tea(expect_events, received_events)
        else:
            return {'success':'False','info':'invalid type for expect_events'}

    def verify_tea(self, expect_events, received_events):
        events_num = len(received_events)
        if events_num == 0:
            return self.__generate_result(-1, expect_events, received_events)
        exception = []
        match_num = 0
        for i in received_events:
            try:
                validate(i, expect_events)
                match_num += 1
            except ValidationError as e:
                Logger.ERROR(e)
                exception.append(e.message)
        return self.__generate_result(match_num, expect_events, received_events, exception=exception)


if __name__ == '__main__':
    """测试代码"""
    print "test"