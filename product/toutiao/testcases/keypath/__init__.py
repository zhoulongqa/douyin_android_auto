__all__ = [
    'KeyPathAnswerDetail',
    'KeyPathShortVideoDetail',
    'KeyPathVideoDetail',
    'KeyPathShortVideoDetail',
    'KeyPathPostsDetail',
    'KeyPathLogin',
    'KeyPathSearch',
]