# -*- coding: utf-8 -*-
import unittest
from product.toutiao.base.BaseTest import BaseTest
from product.toutiao.pages.MainPage import MainPage


class KeyPathSearch(BaseTest):
    def setUp(self):
        """
        前置操作写在这里
        """

    def tearDown(self):
        """
        恢复初始状态操作写在这里，如果没有多个Case，不需要写
        """
        self.uihelper.sleep(1)

    def test_01(self):
        self.uihelper.enter(MainPage['搜索框'], 'something')
        self.uihelper.click_element(MainPage['搜索'])
        self.uihelper.click_element(MainPage['搜索'])
        self.uihelper.sleep(2)
        self.uihelper.click_element(MainPage['搜索结果'])


if __name__ == "__main__":
    unittest.main(verbosity=2)