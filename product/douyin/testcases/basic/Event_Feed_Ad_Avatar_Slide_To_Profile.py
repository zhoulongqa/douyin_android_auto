# -*- coding: utf-8 -*-
import unittest
from product.douyin.base.BaseTest import BaseTest
from utils.eventutils import EventCheck
from product.douyin.pages.MainPage import MainPage
from product.douyin.testcases.commonprocess import PublicProcess
from utils import CommFun


class Event_Feed_Ad_Avatar_Slide_To_Profile(BaseTest):
    def setUp(self):
        """
        埋点事件前置操作写在这里
        """

        #set admock rule



        PublicProcess.change_ip(self)
        CommFun.sleep(5)
        self.uihelper.swipe_up()
        self.uihelper.swipe_up()
        self.uihelper.click_screen_coordinate(200, 200)


    def tearDown(self):
        """
        恢复初始状态操作写在这里，如果没有多个Case，不需要写
        """

    @EventCheck.check_step(event='Event_Feed_Ad_Avatar_Slide_To_Profile', wait_time=5)
    def test_01(self):
        self.uihelper.swipe_left()

if __name__ == "__main__":
    unittest.main(verbosity=2)