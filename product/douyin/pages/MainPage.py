# -*- coding: UTF-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from product.douyin.pages import package_name
from selenium import webdriver


MainPage = {
    # 导航栏
    '推荐': package_name + ':id/tv_hot',
    '附近': package_name + ':id/tv_fresh',
    '直播聚合': package_name + ':id/iv_btn_story_switch',
    '直播头像':package_name + 'id/item_live_name',

    #升级弹窗
    '以后再说': package_name + ':id/later_btn',
    '立即升级': package_name + ':id/update_btn_text',

    #通知栏
    '下载任务': package_name + ':id/desc',

    #下载弹窗
    '下载确定按钮': package_name + 'id/button1',
    '下载取消按钮': package_name + 'id/button2',


    # Feed流
    '头像': package_name + ':id/user_avatar',
    '昵称': package_name + ':id/title',
    '标题': package_name + ':id/desc',
    '点赞': package_name + ':id/digg_container',
    '评论': package_name + ':id/comment_container',
    '分享': package_name + ':id/share_container',
    '音乐': package_name + ':id/music_cover',
    '挑战': package_name + ':id/challenge_2',
    '购物车': package_name + '(shoppingCartButton)',
    '蓝条': package_name + ':id/old_ad_bottom_label_view',
    '新蓝条': package_name + ':id/new_ad_bottom_label_view',
    # '蓝条': package_name + ':id/download_status_view',
    '关注按钮': package_name + ':id/follow_container',
    '半屏落地页': package_name + ':id/ad_half_landpage_container',
    '半屏落地页关闭': package_name + ':id/close_iv',

    # 底Tab
    '首页': ('首页', 'TEXT'),
    '关注': ('关注', 'TEXT'),
    '消息': ('消息', 'TEXT'),
    '加号': '(btn_home_add)',
    '我': ('我', 'TEXT'),

    # 评论区
    '首评论头像': package_name + ':id/avatar',
    # '首评论昵称': package_name + ':id/title',
    '首评论昵称': ('我是评论昵称', 'TEXT'),
    '首评论标题': package_name + ':id/content',
    '首评论标题2': package_name + ':id/content_ll',
    '尾评论头像': package_name + ':id/comment_ad_user_avatar_iv',
    '尾评论昵称': package_name + ':id/comment_ad_label_source_tv',
    '尾评论标题': package_name + ':id/comment_ad_label_des_tv',
    '尾评论区按钮': package_name + ':id/comment_ad_btn',
    '尾评论叉号': package_name + ':id/comment_ad_close_btn',

    # 个人页
    '个人页关注按钮': package_name + ':id/profile_btn_extra',
    '个人页取消关注按钮': package_name + ':id/follow_iv',
    '个人页灰条头像': package_name + ':id/ad_bottom_avatar',
    '个人页灰条昵称': package_name + ':id/ad_bottom_title_tv',
    '个人页灰条标题': package_name + ':id/txt_homepage_bottom_textual',
    '个人页按钮': package_name + ':id/ad_bottom_more_btn',
    '个人页灰条': package_name + ':id/ad_bottom_desc_ll',
    '发消息': package_name + ':id/send_message_btn',
    '个人页返回': package_name + ':id/back_btn',

    # 蒙层表单
    '提交按钮': '//android.view.View/android.widget.Button',
    '填写姓名': '//android.view.View/android.widget.EditText[0]',
    '填写电话': '//android.view.View/android.widget.EditText[0]',
    '查看详情按钮': package_name + ':id/feed_ad_download',
    '重播按钮': package_name + ':id/feed_ad_replay',

    # 蒙层
    '蒙层按钮': package_name + ':id/feed_ad_download',

    #下载落地页
    '按钮': package_name + ':id/download_status',

    # 账密登录页面
    '手机号输入框': package_name + ':id/edit_phone',
    '密码输入框': package_name + ':id/edit_pass',
    '确认按钮': package_name + ':id/btn_login',
    '登录页返回按钮': package_name + ':id/img_btn',

    # 验证码登录页
    '密码登录按钮': package_name + ':id/txt_password',
    '头条登录': package_name + ':id/img_plat_toutiao',
    '微信登录': package_name + ':id/img_plat_weixin',
    'QQ登录': package_name + ':id/img_plat_qq',
    '微博登录': package_name + ':id/img_plat_weibo',
    'X按钮': package_name + ':id/txt_cancel',


    # 我的页
    '更多按钮': package_name + ':id/more_btn',

    # 更多设置弹窗
    '设置': ('设置', 'TEXT'),

    #设置页
    # '测试按钮': package_name + ':id/test',
    '测试按钮': ('Debug Test', 'TEXT'),
    '退出登录按钮': package_name + ':id/logout',


    # 系统设置
    '日志服务器地址': package_name + ':id/host_input',
    '确定': package_name + ':id/host_ok',
    '返回按钮': package_name + ':id/back_btn',


    # # 微头条
    # '微头条列表': '//XCUIElementTypeCell[@name="(TTUGCU13Cell)"]',
    # '微头条详情分享': '(shareButton)',
    #
    # # 小视频
    # '小视频播放': '//XCUIElementTypeOther[@name="(imageView)"]',
    # '小视频作者': '(actionButton)',
    #
    # # 西瓜视频
    # '视频作者': '(themedLabel)',
    # '列表播放视频': '(playButton)',
    # '评论': '(enable)',
    # '写评论': '写评论...',
    # '视频评论输入框': '(internalTextView)',
    #
    # # 文章详情
    # '评论输入框': '(inputTextView)',
    # '发布评论': '(publishButton)',
    # '视频播放返回': 'shadow lefterback titlebar',
    # '返回': '(backButton)',
    #
    # # 底Tab
    # '首页': '首页',
    # '西瓜视频': '西瓜视频',
    # '微头条': '微头条',
    # '小视频': '小视频',
    #
    # # 登录
    # '登录返回': 'lefterbackicon titlebar',
    # '电话登录': '(phoneButton)',
    # '退出登录': '退出登录',
    # '确认退出': '确认退出',
    # '今日阅读XX分钟': '(commonHasLoginEntranceView)',
    # '已登录账号':'Z12B341w1',
    #
    # '收藏': '(favBtn)',
    # '历史': '(historyBtn)',
    # '夜间': '(nightSwitchBtn)',
    # '消息通知': '消息通知',
    # '我的关注': '我的关注',
    # '头条商城': '头条商城',
    # '京东特供': '京东特供',
    # '用户反馈': '用户反馈',
    # '系统设置': '系统设置',
    #
    # # 登录页面
    # '关闭X号': '(close_sdk_login)',
    # '手机号输入框': '(field)',
    # '密码输入框': '//XCUIElementTypeSecureTextField[@name="(field)"]',
    # '账号密码登录': '(switchButton)',
    # '免密码登录': '(switchButton)',
    # '进入头条': '进入头条',
    # '同意CheckBox': '(checkButton)',
    #
    # # 系统设置
    # '高级调试': '高级调试',
    # '日志服务器地址': '日志服务器地址',
    # '地址输入框': '(textFieldView)',
    # '关闭': '关闭',
    # 'Done': 'Done',
    # '确定': '确定',
    #
    # # 问答
    # '更多按钮': 'new more titlebar',
    # '问答详情进入': ('XCUIElementTypeCell', 'CLASS_NAME'),
    # '视频播放': '(videoPlayButton)',
    # '下一个回答': '(nextButton)',
}

