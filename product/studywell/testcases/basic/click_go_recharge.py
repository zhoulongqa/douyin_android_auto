# -*- coding: utf-8 -*-
#核心埋点:确认购买页面
import unittest
from product.studywell.base.BaseTest import BaseTest
from utils.eventutils import EventCheck
from product.studywell.pages.MainPages import MainPage
#需要遍历寻找相应的挑战


class click_go_recharge(BaseTest):
    def setUp(self):
        """
        埋点事件前置操作写在这里
        """
        self.uihelper.click_element(MainPage['启动挑战'])
        challengelist=[MainPage['第一个挑战'],MainPage['第二个挑战'],MainPage['第三个挑战'],MainPage['第四个挑战']]
        for i in challengelist:
            self.uihelper.click_element(i)
            istrue=self.uihelper.find_element(MainPage['付费参加挑战'])
            if istrue==None:
                self.uihelper.swipe_right(1)
                continue
            else:
                break
        self.uihelper.click_element(MainPage['收费挑战'])
    def tearDown(self):
        """
        恢复初始状态操作写在这里，如果没有多个Case，不需要写
        """

    @EventCheck.check_step(event='click_go_recharge', wait_time=5)
    def test_01(self):
        self.uihelper.click_element(MainPage['挑战去充值'])
        self.uihelper.sleep(2)


if __name__ == "__main__":
    unittest.main(verbosity=2)