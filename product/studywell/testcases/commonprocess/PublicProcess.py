# -*- coding: utf-8 -*-
import sys,re,os
reload(sys)
sys.setdefaultencoding('utf-8')
from utils.CommFun import Logger
from product.studywell.pages.MainPages import MainPage
from utils.eventutils.EventHttpHandler import *
from product.studywell.configs.AccountConfig import AccountConfig
import datetime
import time



func_map = {
    '查询挑战': 'seach_challenge',
    '上报埋点地址': 'changeip',
    '滑动查找': 'swipe_and_find',
    '测试准备': 'prepare',
    '点击坐标': 'click_screen',
    '查询点击返回按钮': 'find_click_back_button',
    '等待控件加载后点击': 'wait_and_find_click',
    '检查登录是否丢失': 'check_login_state',
    '开始播放': 'start_time',
    'scheme跳转': 'use_scheme_enter',
    '用户登录': 'new_login',
    '免费专辑购买': 'article_buy_free',
    '视频播放点击返回': 'leave_video_detail',
    '关闭通知栏':  'close_notification_bar'
}

def close_notification_bar(self):
    # screen_size=self.uihelper.getsize()
    screen_size = (375,677)
    print(screen_size,type(screen_size))
    Logger.INFO('screen_size:',screen_size)
    # self.uihelper.close_notification_bar()
    self.driver.flick(screen_size[0] / 2, 0, screen_size[0] / 2, screen_size[1])
    time.sleep(1)


def leave_video_detail(self,x=20,y=23):
    is_replay=self.uihelper.find_element(MainPage['重播按钮'])
    if is_replay is not None:
        self.uihelper.click_element(MainPage['重播按钮'])
    self.uihelper.click_element(MainPage['视频播放区域'])
    self.uihelper.click_element(MainPage['视频播放区域'])
    self.uihelper.click_screen_coordinate(x, y)

def article_buy_free(self):
    is_buy=self.uihelper.find_element(MainPage['购买按钮'])
    if is_buy is not None:
        self.uihelper.click_element(MainPage['购买按钮'])
        time.sleep(1)
        self.uihelper.click_element(MainPage['确认支付'])
        time.sleep(4)
    else:
        pass

def new_login(self):
    self.uihelper.click_element(MainPage['我的'],time=1)
    time.sleep(1)
    islogin = self.uihelper.find_element(MainPage['立即登录'])
    is_login_page=self.uihelper.find_element(MainPage['输入手机号码'], time=1)
    if islogin is not None:
        self.uihelper.click_element(MainPage['立即登录'], time=1)
        run_login(self)
    elif is_login_page is not None:
        run_login(self)
    else:
        is_online = self.uihelper.find_element(MainPage['线上账号'])
        if is_online is not None:
            pass
        else:
            self.uihelper.click_element(MainPage['设置'],time=1)
            self.uihelper.click_element(MainPage['退出登录'],time=1)
            self.uihelper.click_element(MainPage['确定退出登录'])
            self.uihelper.click_element(MainPage['立即登录'], time=1)
            run_login(self)


def use_scheme_enter(self , schemeurl="" , name="course"):
    if name is 'course':
        a = 'sslocal://course?'
    if name is 'book':
        a = 'sslocal://book?'
    if name is  'article':
        a = 'sslocal://article?'
    if name is 'video':
        a = 'sslocal://video?'
    schemeurl = a + schemeurl
    self.uihelper.click_element(MainPage['我的'])
    self.uihelper.swipe_up(1)
    self.uihelper.click_element(MainPage['设置'])
    self.uihelper.click_element(MainPage['高级调试'])
    self.uihelper.swipe_up(1)
    self.uihelper.click_element(MainPage['打开scheme'])
    self.uihelper.click_element(MainPage['scheme输入'])
    self.uihelper.enter(MainPage['scheme输入'], schemeurl)
    self.uihelper.click_element(MainPage['scheme确定'])
    time.sleep(2)


def wait_and_find_click(self,elementName):
    i = 0
    while True:
        self.uihelper.sleep(1)
        istrue = self.uihelper.find_element(MainPage[elementName])
        if istrue is not None:
            print("find it")
            self.uihelper.click_element(MainPage[elementName])
            self.uihelper.sleep(1)
            break
        elif i > 4:
            Logger.INFO("等待5秒仍未加载出来:"+MainPage[elementName])
            break
        else:
            i = i+1
            continue


def check_login_state(self):
    """
    在需要登录的地方校验是否登录状态丢失，跳转到登录页面
    :param self:
    :return: Bool
    """
    is_true=self.uihelper.find_element(MainPage['输入手机号码'], time=1)
    if is_true is not None:
        run_login(self)
    else:
        pass


def run_login(self):
    '''
    登录页执行登录操作
    :param self:
    :return:
    '''
    self.uihelper.click_element(MainPage['输入手机号码'], time=1)
    self.uihelper.enter(MainPage['输入手机号码'], AccountConfig.account)
    self.uihelper.sleep(1)
    self.uihelper.click_element(MainPage['获取验证码'])
    self.uihelper.sleep(3)
    self.uihelper.click_element(MainPage['输入验证码'])
    self.uihelper.send_key_event(1,2,3,4)
    self.uihelper.sleep(3)



def login(self):
    """
        登录流程
        :param self:
        :param phone: 电话号
        :param password: 密码
        :return: Bool
        """
    self.uihelper.click_element(MainPage['我的'], time=1)
    islogin = self.uihelper.find_element(MainPage['立即登录'])
    if islogin is not None:
        self.uihelper.click_element(MainPage['立即登录'], time=1)
        run_login(self)
        return True


def check_online(self):
        is_online = self.uihelper.find_element(MainPage['线上账号'])
        if is_online is not None:
            return True
        elif is_online is None:
            return False
        else:
            return False


def changeip(self,host):
    """
        修改日志上报地址流程
        :param self:
        :param max_scroll_times: 最大滑动次数
        :param host: 上报地址 e.g.:  10.2.193.190:10303
        :return: Bool
        """
    # isme=self.uihelper.find_element(MainPage['我的'])
    # if isme is not None:
    self.uihelper.click_element(MainPage['我的'])
    time.sleep(1)
    is_loginout=self.uihelper.find_element(MainPage['输入手机号码'], time=1)
    if is_loginout is not None:
        # self.uihelper.click_element(MainPage['登录页面返回'])
        self.uihelper.swipe_right(1)
    self.uihelper.click_element(MainPage['设置'])
    self.uihelper.click_element(MainPage['高级调试'])
    self.uihelper.sleep(1)
    self.uihelper.click_element(MainPage['日志host'])
    self.uihelper.sleep(1)
    self.uihelper.enter(MainPage['日志host'], host)
    self.uihelper.sleep(1)
    self.uihelper.click_element(MainPage['Done'])

def click_vip_challenge(self):
    self.uihelper.get_element_name(MainPage['参加收费挑战去'])
    pass
def seach_challenge(self,challengetype):
    """
        查询挑战
        :param self:
        :param max_scroll_times: 最大滑动次数
        :param host: 上报地址 e.g.:  10.2.193.190:10303
        :return: Bool
        """
    challengelist = [MainPage['第一个挑战'], MainPage['第二个挑战'], MainPage['第三个挑战'], MainPage['第四个挑战']]
    for i in challengelist:
        self.uihelper.click_element(i)
        self.uihelper.sleep(2)
        istrue = self.uihelper.find_element(MainPage[challengetype])
        if istrue == None:
            self.uihelper.swipe_right(1)
            if i == MainPage['第四个挑战']:
                self.uihelper.swipe_up(1)
            continue
        else:
            break
    self.uihelper.sleep(1)


def swipe_and_find(self,elementName):
    while True:
        self.uihelper.sleep(1)
        istrue = self.uihelper.find_element(MainPage[elementName])
        if istrue is not None:
            Logger.INFO("Find it")
            break
        else:
            self.uihelper.swipe_up(1)
            self.uihelper.sleep(2)
            continue
    self.uihelper.sleep(1)


def click_screen(self,x,y):
    self.uihelper.click_screen_coordinate(x,y)


def change_online(self):
    self.uihelper.click_element(MainPage['我的'])
    self.uihelper.swipe_up(1)
    self.uihelper.click_element(MainPage['设置'])
    self.uihelper.click_element(MainPage['高级调试'])
    self.uihelper.sleep(1)
    self.uihelper.click_element(MainPage['开启正式环境'])
    self.uihelper.sleep(1)

def find_click_back_button(self):
    is_bace_button_one = self.uihelper.find_element(MainPage['订阅详情返回'])
    is_bace_button_two = self.uihelper.find_element(MainPage['订阅book详情返回'])
    if is_bace_button_one is not None:
        self.uihelper.click_element(MainPage['订阅详情返回'])
    elif is_bace_button_two is not None:
        self.uihelper.click_element(MainPage['订阅book详情返回'])
    else:
        click_screen(28,42)

def prepare(self):
    isalert=self.uihelper.find_element(MainPage['启动系统通知'])
    if isalert is not None:
        self.uihelper.click_element(MainPage['启动允许'])
    self.uihelper.sleep(1)
    isgetbook=self.uihelper.find_element(MainPage['领书领取'])
    if isgetbook is not None:
        self.uihelper.click_element(MainPage['领书领取'])
    try:
        self.driver.switch_to.alert.accept()
        CommFun.Logger.INFO('弹窗处理成功')
        CommFun.sleep(2)
        self.driver.switch_to.alert.accept()
        CommFun.Logger.INFO('弹窗处理成功')
    except:
        CommFun.Logger.INFO('无弹窗需要处理')
    CommFun.sleep(1)
    port = CommFun.get_available_port()
    ip = CommFun.get_local_ip()
    host = ip + ":" + str(port)
    os.environ['EVENTSERVER'] = host
    host = os.getenv('EVENTSERVER')
    # "processArguments":{"args": ["-lemon_server_dev_enabled", "False"]}

    process_args = {"args": ["-lemon_server_dev_enabled", "False"]}
    os.environ['PROCESSARGS'] = str(process_args).replace("'", "\"")
    Logger.INFO(host)
    # if login(self):
    #     pass
    # else:
    #     change_online(self)
    changeip(self,host)
    EventHttpServer.PORT = port
    EventHttpServer.start_server(ip)