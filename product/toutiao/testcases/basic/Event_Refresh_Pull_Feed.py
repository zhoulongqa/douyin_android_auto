# -*- coding: utf-8 -*-
from product.toutiao.base.BaseTest import BaseTest
import unittest
from utils.eventutils import EventCheck


class Event_Refresh_Pull_Feed(BaseTest):
    def setUp(self):
        """
        埋点事件前置操作写在这里
        :return:
        """

    def tearDown(self):
        """
        恢复初始状态操作写在这里，如果没有多个Case，不需要写
        :return:
        """

    @EventCheck.check_step(event='Event_Refresh_Pull_Feed', wait_time=5)
    def test_01(self):
        self.uihelper.swipe_down()


if __name__ == '__main__':
    unittest.main(verbosity=2)
