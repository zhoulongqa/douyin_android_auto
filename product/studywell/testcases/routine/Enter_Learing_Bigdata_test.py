# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from product.studywell.base.BaseTest import BaseTest
import unittest
from utils.eventutils import EventCheck
from product.studywell.testcases.EventTemplate import run_actions
from product.studywell.pages.MainPages import MainPage
from utils.YAMLHelper import YAMLHelper
from utils.CommFun import Logger
import os
from os import path
import re
from product.example.testcases.commonprocess import PublicProcess


local_path = path.abspath('.')
if 'testcases' in local_path:
    filepath = re.findall(r'(.*)testcases', local_path, re.M)[0]
else:
    evn_product = os.getenv('PRODUCT')
    filepath = local_path + '/product/' + evn_product
case = YAMLHelper.open_YAML(filepath+'/testcases/yamlcase/Enter_Learning_Bigdata_Learning_Time.yml')

class EventDemo(BaseTest):
    def setUp(self):
        actions = case['SetUp']
        if actions is not None:
            run_actions(self, actions)
        # run_actions(self, actions)
        self.uihelper.sleep(1)

    @EventCheck.check_step(event=case['Schema'], wait_time=case['WaitTime'])
    def test_01(self):
        actions = case['Event']
        if actions is not None:
            run_actions(self, actions)
        # run_actions(self,actions)
        self.uihelper.sleep(1)

    def tearDown(self):
        pass


if __name__ == '__main__':
    unittest.main(verbosity=2)
