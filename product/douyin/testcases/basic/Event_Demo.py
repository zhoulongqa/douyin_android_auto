# -*- coding: utf-8 -*-
import unittest
from product.douyin.base.BaseTest import BaseTest
from utils.eventutils import EventCheck
from product.douyin.pages.MainPage import MainPage
from product.douyin.testcases.commonprocess import PublicProcess
from utils import CommFun


class Event_Feed_Ad_Avatar_Click_To_Profile(BaseTest):
    def setUp(self):
        """
        埋点事件前置操作写在这里
        """

        #set admock rule


        self.uihelper.swipe_up()
        self.uihelper.swipe_up()
        self.uihelper.swipe_up()
        self.uihelper.swipe_up()
        self.driver.open_notifications()
        CommFun.sleep(2)
        self.uihelper.click_element(MainPage['下载任务'])
        CommFun.sleep(2)
        self.uihelper.click_element(MainPage["下载确认按钮"])
        # self.uihelper.click_screen_coordinate(100,100)
        # self.uihelper.click_screen_coordinate(100,100)
        # # if self.driver.is_app_installed("com.winplay.rich11420"):
        # #     self.driver.remove_app("com.winplay.rich11420")
        # # self.driver.install_app("https://dl.01fe.com/android/game11420_poker.apk")
        # PublicProcess.install_app(self, "ctrip.android.view",
        #                           "https://download2.ctrip.com/html5/Ctrip_1168_9013_7_13_2_mSAD.apk")
        # PublicProcess.uninstall_app(self, "ctrip.android.view")

    def tearDown(self):
        """
        恢复初始状态操作写在这里，如果没有多个Case，不需要写
        """

    @EventCheck.check_step(event='Event_Feed_Ad_Avatar_Click_To_Profile', wait_time=5)
    def test_01(self):
        self.uihelper.swipe_left()

if __name__ == "__main__":
    unittest.main(verbosity=2)