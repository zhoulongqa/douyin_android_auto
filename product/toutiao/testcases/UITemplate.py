# -*- coding: utf-8 -*-
from product.toutiao.base.BaseTest import BaseTest
import unittest
from product.toutiao.pages.MainPage import MainPage
from product.toutiao.testcases.commonprocess import PublicProcess
from utils import YAMLHelper
from utils.CommFun import Logger
import os
import re


local_path = os.path.abspath('.')
if 'testcases' in local_path:
    filepath = re.findall(r'(.*)testcases', local_path, re.M)[0]
else:
    evn_product = os.getenv('PRODUCT')
    filepath = local_path + '/product/' + evn_product

case = YAMLHelper.open_YAML(filepath+'/testcases/yamlcase/UIDemo.yml')


class EventDemo(BaseTest):
    def setUp(self):
        actions = case['SetUp']
        if actions is not None:
            for action in actions:
                YAMLHelper.run_actions(self, action, MainPage)
        self.uihelper.sleep(1)

    def test_01(self):
        actions = case['Test']
        if actions is not None:
            for action in actions:
                YAMLHelper.run_actions(self, action, MainPage)

    def tearDown(self):
        pass


if __name__ == '__main__':
    unittest.main(verbosity=2)
