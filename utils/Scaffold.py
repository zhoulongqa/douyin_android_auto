# -*- coding: UTF-8 -*-
import os
import re


def change_product_name(product_name, bundle_id, udid):
    content = ''
    with open('../product/'+product_name+'/base/BaseTest.py', 'r+') as f:
        content = f.read()
    with open('../product/' + product_name + '/base/BaseTest.py', 'w+') as f:
        content = content.replace('example', product_name)
        f.write(content)
    with open('../product/'+product_name+'/testcases/commonprocess/PublicProcess.py', 'r+') as f:
        content = f.read()
    with open('../product/' + product_name + '/testcases/commonprocess/PublicProcess.py', 'w+') as f:
        content = content.replace('example', product_name)
        f.write(content)
    with open('../product/'+product_name+'/testcases/Event_Category_Cate.py', 'r+') as f:
        content = f.read()
    with open('../product/' + product_name + '/testcases/Event_Category_Cate.py', 'w+') as f:
        content = content.replace('example', product_name)
        f.write(content)
    with open('../product/'+product_name+'/testcases/EventTemplate.py', 'r+') as f:
        content = f.read()
    with open('../product/' + product_name + '/testcases/EventTemplate.py', 'w+') as f:
        content = content.replace('example', product_name)
        f.write(content)
    with open('../product/'+product_name+'/configs/AppiumConfig.json', 'r+') as f:
        content = f.read()
    with open('../product/' + product_name + '/configs/AppiumConfig.json', 'w+') as f:
        content = content.replace('example', product_name)
        content = content.replace('com.ss.iphone.InHouse.article.News', bundle_id)
        content = content.replace('1e2af32a1b26b183807a7c71aed3dc845d63a084', udid)
        f.write(content)


def make_new_product(product_name, bundle_id, udid):
    os.mkdir('../product/'+product_name)
    os.system('cp -R  ../product/example/ ../product/'+product_name)
    change_product_name(product_name, bundle_id, udid)


if __name__ == '__main__':
    make_new_product('dongchedi','com.ss.iphone.InHouse.article.News', '1e2af32a1b26b183807a7c71aed3dc845d63a084')
