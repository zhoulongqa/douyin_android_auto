# -*- coding: utf-8 -*-
import os
import importlib
import re


def get_template(product):
    os.environ['PRODUCT'] = product
    local_path = os.path.abspath('.')
    with open(local_path+'/product/'+product+'/testcases/EventTemplate.py', 'r+') as f:
        tmp = f.read()
        return tmp


def add_init(yms_list):
    if not os.path.exists('casetemp'):
        os.system('mkdir casetemp')
    with open('casetemp/__init__.py', 'w+') as f:
        content = '__all__ = '+str(yms_list)
        f.write(content)


def mk_file(content, new):
    with open('casetemp/'+new+'.py', 'w+') as f:
        f.write(content)


def replace_event(template, ymls_list):
    tmp = template.replace('EventDemo', ymls_list[0])
    target_str = re.findall(r'yamlcase/(.*)\.yml', tmp, re.M)[0]
    tmp = tmp.replace(target_str, ymls_list[0])
    mk_file(tmp, ymls_list[0])
    case_num = len(ymls_list)
    if case_num < 2:
        return
    replace_event(template, ymls_list[1:])


def mk_cases(tmp, ymls_list):
    tmp.append(ymls_list[0]+'.'+ymls_list[0])
    case_num = len(ymls_list)
    if case_num < 2:
        return tmp
    mk_cases(tmp, ymls_list[1:])


def import_cases(case_modules, ymls_list):
    a = importlib.import_module(ymls_list[0])
    case_modules.append(eval('a.'+ymls_list[0]))
    case_num = len(ymls_list)
    if case_num < 2:
        return case_modules
    import_cases(case_modules, ymls_list[1:])


def set_yml_case(yml_str, product):
    ymls_list = yml_str[1:-1].split(',')
    template = get_template(product)
    replace_event(template, ymls_list)
    add_init(ymls_list)
    cases = []
    mk_cases(cases, ymls_list)
    return ','.join(cases)


if __name__ == '__main__':
    import sys
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option("-u", "--URL",
                      help="ipa url")
    parser.add_option("-d", "--device",
                      help="device udid")
    parser.add_option("-c", "--cases",
                      help="test cases")
    parser.add_option("-y", "--yml",
                      help="yml test cases")
    parser.add_option("-t", "--task",
                      help="task id")
    parser.add_option("-p", "--product",
                      help="product name")
    options, args = parser.parse_args(sys.argv)
    url = options.URL
    device = options.device
    cases = options.cases
    yml = options.yml
    product = options.product
    task_id = options.task
    # url = 'http://ci.byted.org/view/iOS/job/TT_iOS_News_InHouse_Beta/lastSuccessfulBuild/artifact/Article/build/Release-iphoneos/NewsInHouse.ipa'
    # device = '1e2af32a1b26b183807a7c71aed3dc845d63a084'
    # cases = 'all_cases'
    # yml = '[EventDemo,EventDemo1]'
    # product = 'toutiao'
    # task_id = '1'
    case = set_yml_case(yml, product)
    import StartTest
    StartTest.main(app_url=url, udid=device, case=case, task_id=task_id, product=product)
