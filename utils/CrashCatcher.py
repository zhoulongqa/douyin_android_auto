# -*- coding: UTF-8 -*-
import os
import Logger
import subprocess
import re
import requests
import json
import time


apps = {
    'NewsInHouse': ['XWTT', 'guohao.qa'],
    'AutoMobileInHouse': ['CARIOS', 'shanxiaolan'],
    'VideoInHouse': ['TTVIDEOI', 'wangqi.seven'],
    'Lemon': ['LEARNING', 'wangduo.vip'],
    'StockInHouse': ['STOCK', 'wangziyan'],
    'EyeU': ['EYEUIOS', 'guohao.qa'],
    'wendaInHouse': ['WUKONG', 'xietianzhen'],
    'JellyGameInHouse': ['GAMEIOS', 'chenghaiyan'],
    'AwemeInhouse': ['AME', 'zhoulong.qa'],
    'NewsLiteInHouse': ['LITE', 'lilingyun'],
}


def get_crash_num(bundle_name, udid, folder='crashlog'):
    if not os.path.exists(folder):
        os.mkdir(folder)
    os.system('idevicecrashreport -u %s -e %s' % (udid, folder))
    num = subprocess.Popen('ls -lR crashlog/|grep %s|wc -l' % bundle_name,
                           stdout=subprocess.PIPE,shell=True).communicate()
    return num[0]


def catch_crash(*dargs, **dkargs):
    """
    :param dargs:
    :param dkargs: bundle_name, udid, folder
    :return: decoration
    """
    def wrapper(func):
        def _wrapper(*args, **kwargs):
            before = get_crash_num(dkargs['bundle_name'], dkargs['udid'])
            func(*args, **kwargs)
            after = get_crash_num(dkargs['bundle_name'], dkargs['udid'])
            if after > before:
                Logger.ERROR('有Crash出现,请检测crashlog文件内.ips日志')
        return _wrapper
    return wrapper

def create_jira_bug(description='', assignee='guohao.qa', summary='[iOSAutoCrash]',
                    reporter='guohao.qa', label='iOSAutoCrash', project='XWTT'):
    url = 'https://rocket.bytedance.net/CreateJIRA'
    headers = {'content-type': 'application/json'}
    content = {
        "project": project,
        "summary": summary,
        "description": description,
        "label": label,
        "assignee": assignee,
        # "components": components,
        "reporter": reporter,
    }
    res = requests.post(url, data=json.dumps(content), headers=headers)
    return res


def upload_crash(file_path):
    url = 'https://rocket.bytedance.net/UploadToInhouseTOS?relative_path=iOSMonkeyCrash/'
    files = {
        'file': open(file_path, 'rb')
    }
    res = requests.post(url, files=files).content
    res_json = json.loads(res)
    if res_json['result'] == 'success':
        return res_json['data']
    else:
        return False


def create_all_bugs(file_path='crashlog', jenkins_path='  '):
    os.chdir(file_path)
    local_path = os.path.abspath('.')
    all_crash = os.listdir('./')
    # print local_path
    tos_url = []
    for crash in all_crash:
        tos_url.append(upload_crash(local_path+'/'+crash))
    for url in tos_url:
        app = url.split('/')[-1].split('-')[0]
        if '.' in app:
            app = app.split('.')[0]
        if app in apps:
            project = apps[app][0]
            reporter = apps[app][1]
        else:
            project = apps['NewsInHouse'][0]
            reporter = apps['NewsInHouse'][1]
        create_jira_bug(description=url+'    '+jenkins_path, reporter=reporter, assignee=reporter, project=project,
                        summary='[iOSAutoCrash]'+app)
        time.sleep(5)
        print '项目:'+project+'\n'+'报告人:'+reporter+'\n'


if __name__ == '__main__':
    test = get_crash_num('NewsInHouse', '1e2af32a1b26b183807a7c71aed3dc845d63a084')
    os.system("rm crashlog/Retired/stacks*.ips")
    test2 = get_crash_num('NewsInHouse', '1e2af32a1b26b183807a7c71aed3dc845d63a084')
    if test2 > test:
        print '11111'
