# -*- coding: utf-8 -*-
import unittest
from product.douyin.base.BaseTest import BaseTest
from utils.eventutils import EventCheck
from product.douyin.pages.MainPage import MainPage
from utils import CommFun
from product.douyin.testcases.commonprocess import PublicProcess


class Event_Comment_End_Ad_Download_Button_Click_Install(BaseTest):
    def setUp(self):
        """
        埋点事件前置操作写在这里
        """
        #set admock rule

        PublicProcess.uninstall_app(self, "ctrip.android.view")

        #连接日志服务
        PublicProcess.change_ip(self)
        CommFun.sleep(5)
        self.uihelper.swipe_up()
        self.uihelper.swipe_up()
        self.uihelper.swipe_up()
        self.uihelper.click_screen_coordinate(200,200)
        CommFun.sleep(2)
        self.uihelper.click_element(MainPage['评论'])
        CommFun.sleep(2)
        self.uihelper.swipe_up()
        CommFun.sleep(2)
        self.uihelper.click_element(MainPage['尾评论区按钮'])
        CommFun.sleep(60)



    def tearDown(self):
        """
        恢复初始状态操作写在这里，如果没有多个Case，不需要写
        """

    @EventCheck.check_step(event='Event_Comment_End_Ad_Download_Button_Click_Install', wait_time=5)
    def test_01(self):
        self.uihelper.click_element(MainPage['尾评论区按钮'])


if __name__ == "__main__":
    unittest.main(verbosity=2)