# -*- coding: utf-8 -*-
import unittest
from product.studywell.base.BaseTest import BaseTest
from product.studywell.testcases.commonprocess import PublicProcess
from utils.eventutils.EventHttpHandler import *
# from Event_Main import run_actions
import re,os
# from product.studywell.configs.AccountConfig import AccountConfig


import os


class Prepare(BaseTest):
    def setUp(self):
        """
        test
        :return:
        """

    def tearDown(self):
        """
        test2
        :return:
        """

    def test_prepare(self):
        # self.uihelper.popup_window_detect_and_click()
        try:
            self.driver.switch_to.alert.accept()
            CommFun.Logger.INFO('弹窗处理成功')
            CommFun.sleep(2)
            self.driver.switch_to.alert.accept()
            CommFun.Logger.INFO('弹窗处理成功')
        except:
            CommFun.Logger.INFO('无弹窗需要处理')
        CommFun.sleep(1)
        # PublicProcess.login(self, phone=AccountConfig.account, password=AccountConfig.password)
        port = CommFun.get_available_port()
        ip = CommFun.get_local_ip()
        host = ip + ":" + str(port)
        os.environ['EVENTSERVER'] = host
        host = os.getenv('EVENTSERVER')
        print(host)
        PublicProcess.changeip(self,host)
        EventHttpServer.PORT = port
        EventHttpServer.start_server(ip)


if __name__ == "__main__":
    unittest.main(verbosity=2)