# -*- coding: UTF-8 -*-

import sys
from os import path
basedir = path.realpath(path.join(path.dirname(__file__), '../../../..'))
sys.path.append(basedir)
reload(sys)
import urllib2
import json

from utils import Logger

STF_URL = "http://thub.byted.org"
GET_DEVICE = STF_URL + "/api/v1/devices"
USE_DEVICE= STF_URL + "/api/v1/user/devices"
STOP_USE_DEVICE = STF_URL + "/api/v1/user/devices/%s"

##如何生成认证token
##进入stf网页端，进入设置（settings），左侧keys tab，输入任意token名字，点击下方generate new key
## clientAuto@bytedance.com

AUTHOR = "f8a92ada27a941e790b35b5d63a1805ce36d904f6d6c42bba6e0b9ea99333c84"

AUTHOR_DICT={
    "TT_Android_Event_Test":"03aed4837bc74c889d129000b99d3e52c0b5977f84f4411495af8273837e4b21",
    "shijian":"83291a88619145e8bebe69bbef566b1e959d336b350844e08ab84e8cc4ee92c0"
}
def get_device_info(serial):
    request = urllib2.Request(GET_DEVICE)
    request.add_header('Authorization', "Bearer %s" % AUTHOR)
    response = urllib2.urlopen(request)
    try:
        devices = json.loads(response.read())
        if devices['success']:
            for device in devices["devices"]:
                if device['serial'] == serial:
                    return device
    except:
        import traceback
        traceback.print_exc()
    return None


def get_target_device(expert_device_arr):
    target_devices = []
    target_devices_ipport = []
    request = urllib2.Request(GET_DEVICE)
    request.add_header('Authorization',"Bearer %s"%AUTHOR)
    response = urllib2.urlopen(request)

    try:
        devices = json.loads(response.read())
        if devices['success']:
            for device in devices["devices"]:
                # print device['ready'],device['using'],device['present'],device['serial']
                if device['ready'] and not device['using'] and device['present']:
                    ip_port = device['display']['url'][5:]
                    ip,port = ip_port.split(":")
                    port = int(port) + 1
                    ip_port = str(ip) + ":" + str(port)
                    if expert_device_arr.count(device['serial']) > 0:
                        target_devices.append(device['serial'])
                        target_devices_ipport.append(ip_port)
                    pass
            if len(target_devices) > 0:
                import random
                index = len(target_devices)*random.Random().random()
                return target_devices[int(index)], target_devices_ipport[int(index)]
            else:
                return None,None
    except:
        import traceback
        traceback.print_exc()
        return None,None
    return None,None

def get_available_device_list():
    target_devices = []
    request = urllib2.Request(GET_DEVICE)
    request.add_header('Authorization', "Bearer %s" % AUTHOR)
    response = urllib2.urlopen(request)

    try:
        devices = json.loads(response.read())
        if devices['success']:
            for device in devices["devices"]:
                # print device['ready'], device['using'], device['present'], device['serial']
                if device['ready'] and not device['using'] and device['present']:
                    target_devices.append(device['serial'])
                    pass
            else:
                return target_devices
    except:
        import traceback
        traceback.print_exc()
        return None
    Logger.INFO(target_devices)
    return target_devices


def start_use_device(udid,timeout = 1800000):
    if udid is None:
        return False
    print udid
    request = urllib2.Request(USE_DEVICE)
    request.add_header('Authorization',"Bearer %s"%AUTHOR)
    request.add_header('Content-Type',"application/json")
    request.add_data('{"serial":"%s","timeout":%s}'%(udid,timeout))

    try:
        response = urllib2.urlopen(request)
        ret = json.loads(response.read())
        if not ret['success']:
            print ret['description']
        return ret['success']

    except BaseException as e:
        import traceback
        traceback.print_exc()
    except urllib2.HTTPError:
        import traceback
        traceback.print_exc()
    return False

def get_device_and_start_use(expert_device_arr,timeout=1800000):
    return start_use_device(get_target_device(expert_device_arr),timeout=timeout)

def stop_use_device(serial):
    if serial is None:
        return False

    request = urllib2.Request(STOP_USE_DEVICE % serial)
    request.add_header('Authorization', "Bearer %s" % AUTHOR)
    request.get_method = lambda: 'DELETE'
    try:
        response = urllib2.urlopen(request)
        return json.loads(response.read())['success']
    except BaseException:
        import traceback
        traceback.print_exc()
    except urllib2.HTTPError:
        import traceback
        traceback.print_exc()
    return False

if __name__ == '__main__':
    # serial = get_target_device(['84B7N16525001203','015bdc43897a4b8f','ENU7N15A30004089','TA00403CZD'])
    # print serial
    print get_available_device_list()
    # start_use_device("00b15e5dd0949287")
    print 'test'
    # import time
    # time.sleep(10)
    # print stop_use_device(serial)
    pass

