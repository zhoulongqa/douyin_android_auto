# -*- coding: UTF-8 -*-
import json
import time
import os
import sys
from appium import webdriver
from os import path
from utils import Logger
import re
basedir = path.realpath(path.join(path.dirname(__file__), '..'))


class AppiumHelper(object):
    def __init__(self):
        self.appium_driver = None
        self.Logger = Logger

    @staticmethod
    def get_appium_config(platform="iOS", product="toutiao"):
        """从json配置文件中读取desired capabilities配置"""
        local_path = path.abspath('.')
        if 'testcases' in local_path:
            filepath = re.findall(r'(.*)testcases', local_path, re.M)[0]
            filepath = filepath+'/configs/AppiumConfig.json'
        else:
            filepath = local_path+"/product/"+product+"/configs/AppiumConfig.json"
        fh = open(filepath)
        obj = json.load(fh)
        if product is not None:
            obj = obj[product]
        if platform is not None:
            obj = obj[platform]
        return obj

    def get_new_webdriver(self, platform="iOS", product="toutiao", addr='127.0.0.1:4723'):
        """从json配置文件中读取配置，获取driver实例"""
        self.Logger.INFO("Connecting to Appium Server")
        udid = os.getenv('DEVICEID')
        evn_product = os.getenv('PRODUCT')
        evn_addr = os.getenv('APPIUMADDR')
        evn_wda = os.getenv('WDAURL')
        evn_process_args = os.getenv('PROCESSARGS')
        evn_event_server = os.getenv('EVENTSERVER')
        evn_device_ip = os.getenv('DEVICEIP')
        evn_device_tt_id = os.getenv('DEVICETTID')
        if evn_addr:
            addr = evn_addr
        if evn_product:
            product = evn_product
        cap = self.get_appium_config(platform=platform, product=product)
        if cap is None:
            raise BaseException('get_new_webdriver:cap is None')
        if udid is not None:
            cap['udid'] = udid
            os.environ['DEVICEID'] = udid
        else:
            os.environ['DEVICEID'] = cap['udid']
        if evn_wda:
            cap['webDriverAgentUrl'] = evn_wda
        if evn_process_args:
            cap['processArguments'] = evn_process_args
        addr = 'http://{0}/wd/hub'.format(addr)
        self.appium_driver = webdriver.Remote(addr, cap)
        self.Logger.INFO("Connected to Appium Server Success")
        return self.appium_driver

    def close_driver(self):
        if self.appium_driver is not None:
            self.appium_driver.quit()

    @staticmethod
    def start_appium_server(appium_port=4723, device="", bp=10000, webdriveragent_port=8100):
        Logger.INFO('-----------------start_appium_server--------------------')
        os.system(
            "appium --log-timestamp -a 127.0.0.1 -p %s -U %s -bp %s --webdriveragent-port %s > %s_log.txt"
            % (appium_port, device, bp, webdriveragent_port, device))

    @staticmethod
    def start_wda_server(webdriveragent_port, udid):
        Logger.INFO('-----------------start_wda_server--------------------')
        # os.system('git clone git@code.byted.org:guohao.qa/WebDriverAgent.git WebDriverAgent')
        # os.system('mkdir WebDriverAgent/Resources')
        # os.system('mkdir WebDriverAgent/Resources/WebDriverAgent.bundle')
        os.system('cp -R /usr/local/lib/node_modules/appium/node_modules/appium-xcuitest-driver/WebDriverAgent WebDriverAgent')
        os.chdir('WebDriverAgent')
        os.system(
            "USE_PORT=%s xcodebuild -project WebDriverAgent.xcodeproj \
           -scheme WebDriverAgentRunner \
           -destination 'id=%s' test" % (str(webdriveragent_port), udid))

    @staticmethod
    def start_iproxy_server(local_port, webdriveragent_port, udid):
        os.system("iproxy %s %s %s" % (local_port, webdriveragent_port, udid))


if __name__ == '__main__':
    from utils import CommFun
    helper = AppiumHelper()
    import multiprocessing

    for a in range(0, 20):
        Logger.INFO("---------------------------------->%s" % (10000 + a))
        appium_process = multiprocessing.Process(target=helper.start_appium_server, args=(10000 + a, 10000 + a))
        appium_process.start()
        CommFun.sleep(5)

    CommFun.sleep(18)
    sys.exit(1)