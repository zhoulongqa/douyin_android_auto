# -*- coding: utf-8 -*-
import unittest
from product.toutiao.base.BaseTest import BaseTest
from product.toutiao.pages.MainPage import MainPage


class KeyPathAnswerDetail(BaseTest):
    def setUp(self):
        """
        前置操作写在这里
        """

    def tearDown(self):
        """
        恢复初始状态操作写在这里，如果没有多个Case，不需要写
        """
        self.uihelper.sleep(1)

    def test_01(self):
        self.uihelper.click_element(MainPage['问答'])
        self.uihelper.swipe_down()
        self.uihelper.click_element(MainPage['问答列表'])
        self.uihelper.click_element(MainPage['问答详情进入'])
        self.uihelper.click_element(MainPage['下一个回答'])


if __name__ == "__main__":
    unittest.main(verbosity=2)