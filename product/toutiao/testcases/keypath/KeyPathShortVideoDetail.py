# -*- coding: utf-8 -*-
import unittest
from product.toutiao.base.BaseTest import BaseTest
from product.toutiao.pages.MainPage import MainPage


class KeyPathShortVideoDetail(BaseTest):
    def setUp(self):
        """
        前置操作写在这里
        """

    def tearDown(self):
        """
        恢复初始状态操作写在这里，如果没有多个Case，不需要写
        """
        self.uihelper.sleep(1)

    def test_01(self):
        self.uihelper.click_element(MainPage['小视频'])
        self.uihelper.swipe_down()
        self.uihelper.click_element_coordinate(MainPage['小视频详情'])
        self.uihelper.swipe_left()
        self.uihelper.click_element(MainPage['微头条详情分享'])


if __name__ == "__main__":
    unittest.main(verbosity=2)