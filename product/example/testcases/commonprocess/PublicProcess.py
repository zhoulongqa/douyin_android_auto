# -*- coding: utf-8 -*-
from product.example.pages.MainPage import MainPage
import time


func_map = {
    '登录流程': 'login',
    '上报地址修改流程': 'change_ip',
}


def is_main_page(self):
    """
    判断是否为首页
    :param self:
    :return:  Bool
    """
    result = self.uihelper.find_element(MainPage['Feed流列表'], time=12)
    print '----------hello--------------'
    if not result:
        print 'False'
        return False
    else:
        print 'True'
        return True


def login(self, phone, password):
    """
    登录流程
    :param self:
    :param phone: 电话号
    :param password: 密码
    :return: Bool
    """
    try:
        self.uihelper.click_element(MainPage['我的'], time=1)
    except:
        self.uihelper.click_element(MainPage['底部未登录'])
    login_flag = self.uihelper.find_element(MainPage['已登录账号'],time=1)
    if login_flag:
        self.uihelper.click_element(MainPage['系统设置'])
        self.uihelper.swipe_up()
        self.uihelper.click_element(MainPage['退出登录'])
        self.uihelper.click_element(MainPage['确认退出'])
        self.uihelper.click_element(MainPage['电话登录'])
        self.uihelper.click_element(MainPage['账号密码登录'])
        self.uihelper.enter(MainPage['手机号输入框'], phone)
        self.uihelper.enter(MainPage['密码输入框'], password)
        self.uihelper.click_element(MainPage['进入头条'])
        time.sleep(1)
    else:
        self.uihelper.click_element(MainPage['电话登录'])
        self.uihelper.click_element(MainPage['账号密码登录'])
        self.uihelper.enter(MainPage['手机号输入框'], phone)
        self.uihelper.enter(MainPage['密码输入框'], password)
        self.uihelper.click_element(MainPage['进入头条'])
        time.sleep(1)
    return True


def change_ip(self, max_scroll_times=5, host=''):
    """
    修改日志上报地址流程
    :param self:
    :param max_scroll_times: 最大滑动次数
    :param host: 上报地址 e.g.:  10.2.193.190:10303
    :return: Bool
    """
    self.uihelper.click_element(MainPage['系统设置'])
    self.uihelper.click_element(MainPage['高级调试'])
    self.uihelper.swipe_up(max_scroll_times)
    self.uihelper.click_element_coordinate(MainPage['地址输入框'])
    self.uihelper.sleep(1)
    self.uihelper.enter(MainPage['地址输入框'],host)
    self.uihelper.click_element(MainPage['Done'])
    self.uihelper.click_element(MainPage['确定'])
    self.uihelper.click_element(MainPage['关闭'])
    return self.uihelper.click_element(MainPage['登录返回'])
