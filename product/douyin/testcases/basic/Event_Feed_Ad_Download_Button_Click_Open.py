# -*- coding: utf-8 -*-
import unittest
from product.douyin.base.BaseTest import BaseTest
from utils.eventutils import EventCheck
from product.douyin.pages.MainPage import MainPage
from utils import CommFun
from product.douyin.testcases.commonprocess import PublicProcess


class Event_Feed_Ad_Download_Button_Click_Open(BaseTest):
    def setUp(self):
        """
        埋点事件前置操作写在这里
        """
        # set admock rule

        PublicProcess.install_app(self, "com.winplay.rich11420", "https://dl.01fe.com/android/game11420_poker.apk")
        CommFun.sleep(2)
        PublicProcess.change_ip(self)
        self.uihelper.swipe_up()
        self.uihelper.swipe_up()
        self.uihelper.click_screen_coordinate(200,200)
        CommFun.sleep(2)




    def tearDown(self):
        """
        恢复初始状态操作写在这里，如果没有多个Case，不需要写
        """

    @EventCheck.check_step(event='Event_Feed_Ad_Download_Button_Click_Open', wait_time=5)
    def test_01(self):
        self.uihelper.click_element(MainPage['蓝条'])





if __name__ == "__main__":
    unittest.main(verbosity=2)