# __author__ = 'guohao'
import json
import unittest
from utils.AppiumHelper import *
from utils.UIHelper import UIHelper
from utils import Logger
from time import sleep
from utils.URLService import IOSUrlService
import re


class BaseTest(unittest.TestCase):
    driver = None
    adb = None
    uihelper = None

    @classmethod
    def setUpClass(cls):
        Logger.INFO("Init Appium Driver")
        appium_client = AppiumHelper()
        cls.driver = appium_client.get_new_webdriver(platform='iOS', product='studywell')
        os.environ['PRODUCT'] = 'studywell'
        cls.driver.implicitly_wait(5)
        cls.ios_url_service = IOSUrlService()
        local_path = path.abspath('.')
        if 'testcases' in local_path:
            filepath = re.findall(r'(.*)testcases', local_path, re.M)[0]
        else:
            filepath = local_path
        cls.uihelper = UIHelper(cls.driver, url_service=cls.ios_url_service, exception_check_json=filepath+'/configs/IOSPopupElements.json')
        sleep(5)
        try:
            cls.driver.switch_to.alert.accept()
        except:
            pass

    @classmethod
    def tearDownClass(cls):
        Logger.INFO("Exit Appium Driver")
        cls.driver.quit()