# -*- coding: UTF-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
# __author__ = 'guohao'
import json
import re
import subprocess
import shlex
from threading import Timer
from difflib import SequenceMatcher
import selenium.common.exceptions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By
from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.common.touch_action import TouchAction
import subprocess
import unittest
import os
from utils import Logger
import time
from utils.adbutils.AdbHelper import AdbHelper

path = os.path.abspath('.')
if 'testcases' in path:
    filepath = os.path.abspath('../')
else:
    filepath = os.path.abspath('.')


class TTBy(MobileBy):
    """
    add by_text
    as for by content-desc, use accessibilit_id
    """
    TEXT = 'TEXT'


func_map = {
        '查找控件': 'find_element',
        '点击控件': 'click_element',
        '点击控件坐标': 'click_element_coordinate',
        '点击屏幕坐标': 'click_screen_coordinate',
        '输入内容': 'enter',
        '点击键盘': 'send_key_event',
        '向上滑动': 'swipe_up',
        '向下滑动': 'swipe_down',
        '向左滑动': 'swipe_left',
        '向右滑动': 'swipe_right',
        '向上滑动点击': 'swipe_up_click_element',
        '向上滑动点击坐标': 'swipe_up_click_element_coordinate',
        '控件内向左滑动': 'swipe_left_in_element',
        '控件内向右滑动': 'swipe_right_in_element',
        '控件内向上滑动': 'swipe_up_in_element',
        '控件内向下滑动': 'swipe_down_in_element',
        '启动应用': 'launch_test_app',
        '关闭应用': 'close_test_app',
        '获取控件值': 'get_element_value',
        '获取控件名': 'get_element_name',
        '获取控件标签': 'get_element_label',
        '断言不等': 'assert_not_equal',
        '断言相等': 'assert_equal',
        '断言存在': 'assert_true',
        '等待': 'sleep',
        '跳转页面': 'go_page',
        '打开通知栏': 'open_notification_bar',
        '关闭通知栏': 'close_notification_bar'
    }


class UIHelper(object):
    """
    Usage:UI
        help user to find element friendly,
        just forgot appium find element method, such as:
        find_element_by_name, find_element_by_xpath,
        find_element_by_accessibility_id
        whatever. we provide one method "find_element" to replace
        appium several find element functions.

        it makes find element more easily
    """

    def __init__(self, appium_driver, url_service, exception_check_json=""):
        """
        :param appium_driver: appium driver
        :param url_service: URL Scheme service
        :param popup_elements: popup elements
        """

        self.log = Logger
        self.driver = appium_driver
        self.udid = self.driver.capabilities['udid']
        self.adb_helper = AdbHelper(self.udid)
        self.exception_check_json = exception_check_json
        self.url_service = url_service
        self.func_map = func_map

    @staticmethod
    def abs_page(source):
        x = re.findall('>\n(\s+<\w+)', source)
        v = [val.strip().replace("<", str(val.count(" "))) for idx, val in enumerate(x)]
        z = [value for idx, value in enumerate(v[:-1]) if v[idx] != v[idx + 1]]
        return "".join(z)

    @staticmethod
    def diff_page(page1, page2):
        return SequenceMatcher(None, UIHelper.abs_page(page1), UIHelper.abs_page(page2)).real_quick_ratio()

    # 查找控件
    def find_element(self, text, time=10):
        """
        :param text:
        :param time:
        :return:
        """
        self.log.INFO("search_element: {}".format(text))
        value = None
        by = None
        # get value and by
        if isinstance(text, str):
            value = text
            if text.startswith("/") or text.startswith("(/"):
                by = By.XPATH
            else:
                by = By.ID
        elif isinstance(text, tuple) and len(text) == 2:
            value = text[0]
            by = text[1]
            try:
                if by == TTBy.TEXT:
                    value = 'new UiSelector().text("' + value + '")'
                    by = TTBy.ANDROID_UIAUTOMATOR
                else:
                    by = getattr(MobileBy, by)
            except:
                raise BaseException("currently only support by in TTBy")
        else:
            raise BaseException("incorrect syntax in pages module")
        # find target_element based on by and value
        try:
            self.log.INFO("----find element by " + str(by))
            target_element = WebDriverWait(self.driver, time).until(
                expected_conditions.presence_of_all_elements_located((by, value)))
            return target_element
        except selenium.common.exceptions.TimeoutException:
            self.log.ERROR('find element fail')
            return None


    def find_element_with_popup(self, text, time=10):
        el = self.find_element(text, time)
        if not el:
            while self.popup_window_detect_and_click():
                el = self.find_element(text, time)
                if el:
                    return el
        return el

    def popup_window_detect_and_click(self, text=""):
        """
        :param text:
        :return: True/False
        """
        self.log.INFO("start handle alert")
        source = self.driver.page_source
        exception_info = self.load_exception_elements()
        for key in exception_info:
            self.log.INFO("checking if popup_element: {} exist".format(key))
            if key in source:
                self.log.INFO("popup_element {} is found".format(key))
                text = exception_info[key]
                exception_btn = self.find_element_with_popup(text, time=5)
                if exception_btn:
                    exception_btn[0].click()
                    time.sleep(2)
                    self.log.INFO(exception_info[key] + ' -- >弹窗处理完成')
                    return True
                else:
                    self.log.INFO('没有找到这个弹框控件： ' + exception_info[key])
                    return False

    def popup_window_detect(self, text=""):
        """
        Usage:
            check if is there a popup window on top activity
            if there is a popup window, handle it, and continue to
            find the target element.
        :param text: the target element text

        :return: the target element
        """
        # 处理无法找到控件的问题， 加载自动处理窗体设置
        self.log.INFO('开始弹框检查')
        time.sleep(1)
        source = self.driver.page_source

        exception_info = self.load_exception_elements()

        # detect is there a popup element exists
        for key in exception_info:
            self.log.INFO('正在检查页面是否有异常元素：' + key)
            if key in source:
                # click the popup elements
                self.log.INFO('Popup Element 发现页面有异常元素: ' + key)

                exception_btn = self.find_element(exception_info[key], time=5)

                if exception_btn:
                    exception_btn[0].click()
                    time.sleep(2)
                    self.log.INFO(exception_info[key] + ' -- >弹窗处理完成')
                    break
                else:
                    self.log.INFO('没有找到这个弹框控件： ' + exception_info[key])
                    source = self.driver.page_source

        if text:
            self.log.INFO('再次尝试寻找这个控件： ' + text)
            # 处理完弹窗以后，再次寻找这个对象
            return self.find_element(text)

    def load_exception_elements(self):
        print(self.exception_check_json)
        return json.load(open(self.exception_check_json))['popupElements']

    def exist(self, text, timeout=10):
        """
        :param text:
        :return: True/False
        """
        if self.find_element(text, timeout):
            return True
        else:
            return False

    def click_element(self, text, time=5, index=0):
        """
        :rtype: object
        """
        elements = self.find_element(text, time=time)
        if elements:
            elements[index].click()
        else:
            self.log.ERROR("控件：" + str(text) + ",获取不到")
            self.get_screenshot()
            raise unittest.TestCase.failureException


    def click_element_coordinate(self, text, wait_time=5, index=0, x_per=0.5, y_per=0.5):
        """
        click element by its coordinate location
        :param text: the target element's text
        :param wait_time: find target element wait time
        :param index: target element's index
        :param x_per: x coordinate percent
        :param y_per: y coordinate percent
        :rtype: object
        """
        elements = self.find_element(text, time=wait_time)
        if elements:
            rect = dict(elements[index].size.items() + elements[index].location.items())
            x = rect['x'] + rect['width'] * x_per
            y = rect['y'] + rect['height'] * y_per
            return self.click_screen_coordinate(x, y)
        else:
            self.log.ERROR("控件：" + text + ",获取不到")
            self.get_screenshot()
            raise unittest.TestCase.failureException


    def swipe_up_click_element(self, text, wait_time=5, index=0, max_time=10):
        """
        :rtype: object
        """
        i = 0
        elements = self.find_element(text, time=wait_time)
        while not elements and i < max_time:
            self.swipe_up_one_page()
            elements = self.find_element(text, time=max_time-i)
            i += 1
        if elements:
            elements[index].click()
        else:
            self.log.ERROR("控件：" + text + ",获取不到")
            self.get_screenshot()
            raise unittest.TestCase.failureException


    def swipe_up_click_element_coordinate(self, text, time=1, index=0, max_time=10, x_per=0.5, y_per=0.5):
        """
        :rtype: object
        """
        i = 0
        while i < max_time:
            # if not self.is_valid_element(elements[index].location.items()):
            elements = self.find_element(text, time=time)
            if not elements[index].is_displayed():
                self.swipe_up()
                i += 1
                continue
            else:
                rect = dict(elements[index].size.items() + elements[index].location.items())
                x = rect['x'] + rect['width'] * x_per
                y = rect['y'] + rect['height'] * y_per
                return self.click_screen_coordinate(x, y)
        self.log.ERROR("控件：" + text + ",获取不到")
        self.get_screenshot()
        raise unittest.TestCase.failureException

    def swipe_left_in_element(self, text, time=5, index=0, x_per=0.6):
        """
        swipe left in element by its coordinate location
        :param text: the target element text
        :rtype: object
        """
        elements = self.find_element(text, time=time)
        if elements:
            rect = dict(elements[index].size.items() + elements[index].location.items())
            x = rect['x'] + rect['width'] * 0.8
            y = rect['y'] + rect['height'] * 0.5
            x2 = rect['width'] * x_per
            # return TouchActions(self.driver).press(x=x, y=y).move_to(x=-x2, y=0).release().perform()
            self.driver.execute_script('mobile: dragFromToForDuration',
                                       {'duration': 1,
                                        'fromX': x,
                                        'fromY': y,
                                        'toX': x2,
                                        'toY': y})
            return True
        else:
            self.log.ERROR("控件：" + text + ",获取不到")
            self.get_screenshot()
            raise unittest.TestCase.failureException

    def swipe_right_in_element(self, text, time=5, index=0, x_per=0.6):
        """
        swipe right in element by its coordinate location
        :param text: the target element text
        :rtype: object
        """
        elements = self.find_element(text, time=time)
        if elements:
            rect = dict(elements[index].size.items() + elements[index].location.items())
            x = rect['x'] + rect['width'] * 0.2
            y = rect['y'] + rect['height'] * 0.5
            x2 = rect['width'] * x_per
            # return TouchActions(self.driver).press(x=x, y=y).move_to(x=x2, y=0).release().perform()
            self.driver.execute_script('mobile: dragFromToForDuration',
                                       {'duration': 1,
                                        'fromX': x,
                                        'fromY': y,
                                        'toX': x2,
                                        'toY': y})
            return True
        else:
            self.log.ERROR("控件：" + text + ",获取不到")
            self.get_screenshot()
            raise unittest.TestCase.failureException

    def swipe_up_in_element(self, text, time=5, index=0, y_per=0.6):
        """
        swipe up in element by its coordinate location
        :param text: the target element text
        :rtype: object
        """
        elements = self.find_element(text, time=time)
        if elements:
            rect = dict(elements[index].size.items() + elements[index].location.items())
            x = rect['x'] + rect['width'] * 0.5
            y = rect['y'] + rect['height'] * 0.8
            y2 = rect['height'] * y_per
            # return TouchActions(self.driver).press(x=x, y=y).move_to(x=0, y=-y2).release().perform()
            self.driver.execute_script('mobile: dragFromToForDuration',
                                       {'duration': 1,
                                        'fromX': x,
                                        'fromY': y,
                                        'toX': x,
                                        'toY': y2})
            return True
        else:
            self.log.ERROR("控件：" + text + ",获取不到")
            self.get_screenshot()
            raise unittest.TestCase.failureException

    def swipe_down_in_element(self, text, time=5, index=0, y_per=0.6):
        """
        swipe down in element by its coordinate location
        :param text: the target element text
        :rtype: object
        """
        elements = self.find_element(text, time=time)
        if elements:
            rect = dict(elements[index].size.items() + elements[index].location.items())
            x = rect['x'] + rect['width'] * 0.5
            y = rect['y'] + rect['height'] * 0.2
            y2 = rect['height'] * y_per
            # return TouchActions(self.driver).press(x=x, y=y).move_to(x=0, y=y2).release().perform()
            self.driver.execute_script('mobile: dragFromToForDuration',
                                       {'duration': 1,
                                        'fromX': x,
                                        'fromY': y,
                                        'toX': x,
                                        'toY': y2})
            return True
        else:
            self.log.ERROR("控件：" + text + ",获取不到")
            self.get_screenshot()
            raise unittest.TestCase.failureException


    def click_screen_coordinate(self, x, y):
        """
        click coordinate location
        :param x:
        :param y:
        :return:
        """
        return TouchAction(self.driver).press(x=x, y=y).release().perform()

    def recovery_to_test_app(self, app_name='头条内测'):
        # 非开发证书的包不能用 先别用
        source = self.driver.page_source
        app_cn_name = app_name
        current_app = re.findall('"XCUIElementTypeApplication" name="(.+)?"\slabel', source)[0]
        self.log.INFO("current_app:[" + current_app + ']' + "target:[" + app_cn_name + ']')
        if not app_cn_name.strip() == current_app.strip():
            self.log.INFO('跳出被测试App，准备恢复')
            shell_cmd = 'idevicedebug -u {uuid} run {bound_id} &'.format(
                uuid=self.driver.capabilities['bundleId'],
                bound_id='com.ss.iphone.InHouse.article.News')
            self.log.INFO('恢复App: ' + shell_cmd)
            os.run(shell_cmd)
        else:
            self.log.INFO('没有跳出App')

    def launch_test_app(self):
        self.driver.launch_app()

    def close_test_app(self):
        self.driver.close_app()

    def background_test_app(self, duration=1):
        self.log.INFO('进入后台'+str(duration)+'秒')
        self.log.INFO('进入后台')
        self.driver.background_app(duration)

    def lock_test_app(self, duration=1):
        # API废弃了 先别用
        self.log.INFO('锁屏' + str(duration) + '秒')
        self.driver.lock(duration)

    @staticmethod
    def run(cmd, timeout_sec=20):
        proc = subprocess.Popen(shlex.split(cmd),
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE)
        kill_proc = lambda p: p.kill()
        timer = Timer(timeout_sec, kill_proc, [proc])
        try:
            timer.start()
            stdout, stderr = proc.communicate()
        finally:
            timer.cancel()
            return stdout if stdout else stderr

    # 获取控件的name
    def get_element_name(self, text, index=0):
        elements = self.find_element(text)
        if elements:
            text1 = elements[index].get_attribute('name').strip()
            return text1
        else:
            self.log.ERROR("控件：" + text + ",获取不到")
            self.get_screenshot()
            raise unittest.TestCase.failureException

    # 获取控件的label
    def get_element_label(self, text, index=0):
        elements = self.find_element(text)
        if elements:
            text1 = elements[index].get_attribute('label').strip()
            return text1
        else:
            self.log.ERROR("控件：" + text + ",获取不到")
            self.get_screenshot()
            raise unittest.TestCase.failureException

    # 获取控件的value
    def get_element_value(self, text, index=0):
        elements = self.find_element(text)
        if elements:
            text1 = elements[index].get_attribute('value').strip()
            return text1
        else:
            self.log.ERROR("控件：" + text + ",获取不到")
            self.get_screenshot()
            raise unittest.TestCase.failureException

    # 截屏
    def get_screenshot(self):
        # 判断文件是否存在，若不存在，创建
        datefile = filepath + '/screenshot'
        if not os.path.exists(datefile):
            os.mkdir(datefile)
        now = time.strftime("%Y-%m-%d-%H-%M-%S", time.localtime(time.time()))
        filename = datefile + '/' + now + ".png"
        time.sleep(1)
        self.driver.get_screenshot_as_file(filename)
        print('screenshot:' + now + '.png')


    @property
    def height(self):
        return self.driver.get_window_size()['height']

    @property
    def width(self):
        return self.driver.get_window_size()['width']

    # 获取屏幕大小
    def getsize(self):
        screen = self.driver.get_window_size()
        x = screen['width']
        y = screen['height']
        self.log.INFO('screen width: '+str(x)+'screen height: '+str(y))
        return (x, y)

    # 滑动
    def swipe_by_percent(self, start_x_per, start_y_per, end_x_per, end_y_per, times=1, scroll_time_length=300):
        while times:
            try:
                height = self.height
                width = self.width

                start_x = width * start_x_per
                start_y = height * start_y_per
                end_x = width * end_x_per
                end_y = height * end_y_per
                self.adb_helper.execute_adb_shell("input swipe %s %s %s %s %s" % (start_x, start_y, end_x, end_y, scroll_time_length))
                times -= 1
                time.sleep(1)
            except BaseException as e:
                Logger.ERROR(e.message)
                Logger.ERROR(e.args)

    # 上滑一页
    def swipe_up_one_page(self):
        Logger.INFO("scrolling up one page")
        return self.swipe_up(x_per=0.5, start_y_per=0.9, end_y_per=0.2)

    # 上滑
    def swipe_up(self, x_per=0.5, start_y_per=0.9, end_y_per=0.1, times=1):
        return self.swipe_by_percent(x_per, start_y_per, x_per, end_y_per, times=times)

    # 下滑
    def swipe_down(self, x_per=0.5, start_y_per=0.2, end_y_per=0.9, times=1):
        return self.swipe_by_percent(x_per, start_y_per, x_per, end_y_per, times=times)

    # 左滑
    def swipe_left(self, y_per=0.5, start_x_per=0.9, end_x_per=0.1, times=1):
        return self.swipe_by_percent(start_x_per, y_per, end_x_per, y_per, times=times)

    # 右滑
    def swipe_right(self, y_per=0.5, start_x_per=0.1, end_x_per=0.9, times=1):
        return self.swipe_by_percent(start_x_per, y_per, end_x_per, y_per, times=times)


    # 判断结果是否符合预期
    def assert_true(self, expr, msg=None):
        if not expr:
            self.get_screenshot()
            raise unittest.TestCase.failureException(msg)
        else:
            return True

    # 相等
    def assert_equal(self, first, second):
        if first != second:
            self.get_screenshot()
            mes = str(first) + '不等于' + str(second)
            raise unittest.TestCase.failureException(mes)

    # 不相等
    def assert_not_equal(self, first, second):
        if first == second:
            self.get_screenshot()
            mes = str(first) + '等于' + str(second)
            raise unittest.TestCase.failureException(mes)

    # 两个数值大于/等于
    def assert_greaterequal(self, first, second):
        if not first >= second:
            self.get_screenshot()
            mes = str(first) + '的值不等于或大于' + str(second) + '的值'
            raise unittest.TestCase.failureException()

    # 在输入框中输入内容
    def enter(self, text, content, index=0):
        elements = self.find_element(text)
        if elements:
            elements[index].clear()
            elements[index].set_value(content)
        else:
            self.log.ERROR("控件：" + text + ",获取不到")
            self.get_screenshot()
            raise unittest.TestCase.failureException

    # 点击键盘按键
    def send_key_event(self, *args):
        for i in args:
            text = ""
            if isinstance(i, int):
                text = str(i)
            elif isinstance(i, unicode):
                text = i.encode('utf-8')
            else:
                text = i
            Logger.INFO("-------点击键盘: "+text)
            self.click_element(text)

    @staticmethod
    def show_object(obj):
        try:
            el_info = " ".join(['text:{text},'.format(text=obj.text),
                                'tag_name: {tag_name},'.format(tag_name=obj.tag_name),
                                'enabled: {enabled},'.format(enabled=obj.is_enabled()),
                                'displayed:{displayed}'.format(displayed=obj.is_displayed())])

        except Exception as e:
            print("获取元素信息失败")

    @staticmethod
    def element_to_string(ios_element, idx):

        if ios_element.name:
            return ios_element.name

        return ios_element.cls_name + " index: " + str(idx)

    def get_all_elements_info(self):
        all_elements_info = []
        el_text = self.driver.page_source

        for text in el_text:
            name = ""
            el_type = re.findall(u'type="(\w+)"\s', text)[0]
            name_info = re.findall(u'name="(.+?)"\s', text)

            # 控件的文字描述
            if name_info:
                name = name_info[0]

            # 特殊的组件，比如iOS状态栏，等，不需要遍历
            if 'XCUIElementTypeOther' in el_type:
                continue

            el_x = int(re.findall(u'x="([\d|-]+)"', text)[0])
            el_y = int(re.findall(u'y="([\d|-]+)"', text)[0])
            el_width = int(re.findall(u'width="([\d|-]+)"', text)[0])
            el_height = int(re.findall(u'height="([\d|-]+)"', text)[0])

            all_elements_info.append(IOSElementInfo(el_type, el_x, el_y, el_height, el_width, name))

        return all_elements_info

    def is_valid_element(self, el_info):

        # iOS 屏幕尺寸
        rect = dict(el_info)
        if rect['y'] > 623:
            self.log.INFO("超出当前页面的元素")
            return False
        else:
            return True
        # if 'back' in el_info.name:
        #     self.log.INFO("可能是返回按钮")
        #     return False

        # if not el.is_enabled():
        #     self.log.INFO("控件不展示")
        #     return False

    @staticmethod
    def sleep(seconds=1):
        Logger.INFO('---------------sleep-----------------%s' % (seconds))
        # if addDB == 1:
        #     from UIAutoTest.Helper.DBHelper import DBHelper
        #     db = DBHelper()
        #     db.insert_into_step_data('','',"等待",bid)
        time.sleep(seconds)

    def go_page(self, url):
        self.log.INFO('----- go page ------'+url)
        result = self.url_service.jump_to_page(url)
        time.sleep(1)
        if result:
            return True
        else:
            self.log.ERROR("url：" + url + ",跳转失败")
            raise unittest.TestCase.failureException


    @staticmethod
    def get_local_device():
        text = subprocess.Popen('ios-deploy -l',
                                shell=True, stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE).stdout.read().strip()
        device_id = re.findall(r'(\w{40})', str(text))
        return device_id[0]

    def get_element_values(self, text, time=10, index=0):
        elements = self.find_element(text, time)
        if elements:
            text1 = elements[index].get_attribute('value')
            return text1
        else:
            self.log.ERROR("控件：" + text + ",获取不到")
            self.get_screenshot()
            raise unittest.TestCase.failureException

        # 打开通知栏
    def open_notification_bar(self):
        screen_size = self.getsize()
        self.driver.flick(screen_size[0] / 2, 0, screen_size[0] / 2, screen_size[1])
        time.sleep(3)

        # 关闭通知栏
    def close_notification_bar(self):
        screen_size = self.getsize()
        self.driver.flick(screen_size[0] / 2, screen_size[1]-5, screen_size[0] / 2, 5)
        time.sleep(3)

class IOSElementInfo:
    def __init__(self, cls_name, x, y, height, width, name=""):
        self.cls_name = cls_name
        self.name = name
        self.x = x
        self.y = y
        self.height = height
        self.width = width
