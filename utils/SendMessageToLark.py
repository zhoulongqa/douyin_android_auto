# coding=utf-8
import json
import logging
import requests


def send_lark_message(lark_messages='',member=''):
    """
    :param lark_messages: string
    :param member: email guohao.qa@bytedance.com
    :return: Bool
    """
    BASE_URL = "https://oapi.zjurl.cn/open-apis/api/v2/message/private_chat"
    data = {
        "token": "b-e57916f0-f644-472e-a9d4-087b39ce70fc",
        # "channel": "6494043538316067086",
        "email": member,
        "msg_type": "text",
        "content": {
            "text": lark_messages
        }
    }

    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    r = requests.post(
        url=BASE_URL,
        data=json.dumps(data),
        headers=headers,
    )
    if r.status_code > 200:
        logging.error("post message to lark-apis failed: r= %s", r)
        return False

    logging.info("post message to lark-channel success: channel= %s text= %s",
                 data.get('channel'), data.get('text'))
    return True


if __name__ == '__main__':
        send_lark_message(lark_messages='test111', member='guohao.qa@bytedance.com')
