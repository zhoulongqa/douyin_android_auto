__all__ = [
    'Event_Go_Detail_Click_Video',
    'Event_Stay_Page_Click_Video',
    'Event_Video_Detail_Play',
    'Event_Video_Feed_Play',
    'Event_Video_Over_Click_Video',
    'Event_Video_Play_Click_Video',
    'Event_Video_Play_Click_Video_Feed',
]