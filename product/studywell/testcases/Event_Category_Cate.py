# -*- coding: utf-8 -*-
import unittest
from product.example.base.BaseTest import BaseTest
from utils.eventutils import EventCheck
from product.example.pages.MainPage import MainPage


class Event_Category_Cate(BaseTest):
    def setUp(self):
        """
        埋点事件前置操作写在这里
        """
        self.uihelper.click_element(MainPage['电影'])

    def tearDown(self):
        """
        恢复初始状态操作写在这里，如果没有多个Case，不需要写
        """

    @EventCheck.check_step(event='Event_Category_Cate', wait_time=5)
    def test_01(self):
        self.uihelper.click_element(MainPage['电影'])


if __name__ == "__main__":
    unittest.main(verbosity=2)