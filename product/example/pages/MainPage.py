# -*- coding: UTF-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')


MainPage = {
    # 导航栏
    '我的': '(mineIconButton)',
    '底部未登录': '未登录',
    '搜索框': '(searchLabel)',
    '搜索结果': '(ssWebView)',
    '搜索': 'Search',
    '发布': '(publishButton)',

    # 频道
    '频道条': '(scrollView)',
    '更多频道': '(expandButton)',
    '关注': '关注',
    '推荐': '推荐',
    '电影': '电影',
    '热点': '热点',
    '问答': '问答',
    '视频': '视频',

    # Feed流
    '刷新按钮': '(refreshButton)',
    'Feed流列表': ('XCUIElementTypeCell', 'CLASS_NAME'),
    '详情播放视频': 'playicon video',
    '问答列表': '(WDNativeListBaseCell)',

    # 微头条
    '微头条列表': '//XCUIElementTypeCell[@name="(TTUGCU13Cell)"]',
    '微头条详情分享': '(shareButton)',

    # 小视频
    '小视频播放': '//XCUIElementTypeOther[@name="(imageView)"]',
    '小视频作者': '(actionButton)',

    # 西瓜视频
    '视频作者': '(themedLabel)',
    '列表播放视频': '(playButton)',
    '评论': '(enable)',
    '写评论': '写评论...',
    '视频评论输入框': '(internalTextView)',

    # 文章详情
    '评论输入框': '(inputTextView)',
    '发布评论': '(publishButton)',
    '视频播放返回': 'shadow lefterback titlebar',
    '返回': '(backButton)',

    # 底Tab
    '首页': '首页',
    '西瓜视频': '西瓜视频',
    '微头条': '微头条',
    '小视频': '小视频',

    # 登录
    '登录返回': 'lefterbackicon titlebar',
    '电话登录': '(phoneButton)',
    '退出登录': '退出登录',
    '确认退出': '确认退出',
    '今日阅读XX分钟': '(commonHasLoginEntranceView)',
    '已登录账号':'Z12B341w1',

    '收藏': '(favBtn)',
    '历史': '(historyBtn)',
    '夜间': '(nightSwitchBtn)',
    '消息通知': '消息通知',
    '我的关注': '我的关注',
    '头条商城': '头条商城',
    '京东特供': '京东特供',
    '用户反馈': '用户反馈',
    '系统设置': '系统设置',

    # 登录页面
    '关闭X号': '(close_sdk_login)',
    '手机号输入框': '(field)',
    '密码输入框': '//XCUIElementTypeSecureTextField[@name="(field)"]',
    '账号密码登录': '(switchButton)',
    '免密码登录': '(switchButton)',
    '进入头条': '进入头条',
    '同意CheckBox': '(checkButton)',

    # 系统设置
    '高级调试': '高级调试',
    '日志服务器地址': '日志服务器地址',
    '地址输入框': '(textFieldView)',
    '关闭': '关闭',
    'Done': 'Done',
    '确定': '确定',

    # 问答
    '更多按钮': 'new more titlebar',
    '问答详情进入': ('XCUIElementTypeCell', 'CLASS_NAME'),
    '视频播放': '(videoPlayButton)',
    '下一个回答': '(nextButton)',
}

