# -*- coding: utf-8 -*-
import unittest
from product.toutiao.base.BaseTest import BaseTest
from product.toutiao.pages.MainPage import MainPage


class KeyPathVideoDetail(BaseTest):
    def setUp(self):
        """
        前置操作写在这里
        """

    def tearDown(self):
        """
        恢复初始状态操作写在这里，如果没有多个Case，不需要写
        """
        self.uihelper.sleep(1)

    def test_01(self):
        self.uihelper.click_element(MainPage['西瓜视频'])
        self.uihelper.swipe_down()
        self.uihelper.click_element(MainPage['评论'])
        self.uihelper.click_element(MainPage['写评论'])
        self.uihelper.enter(MainPage['评论输入框'], 'hahahhahah')
        self.uihelper.click_element(MainPage['发布'])


if __name__ == "__main__":
    unittest.main(verbosity=2)