# -*- coding: utf-8 -*-
from product.toutiao.base.BaseTest import BaseTest
import unittest
from utils.eventutils import EventCheck
from product.toutiao.pages.MainPage import MainPage


class Event_Stay_Page_Click_Video(BaseTest):
    def setUp(self):
        """
        埋点事件前置操作写在这里
        :return:
        """
        self.uihelper.click_element(MainPage['西瓜视频'])
        self.uihelper.click_element(MainPage['西瓜视频'])
        self.uihelper.click_element(MainPage['评论'], index=0)

    def tearDown(self):
        """
        恢复初始状态操作写在这里，如果没有多个Case，不需要写
        :return:
        """

    @EventCheck.check_step(event='Event_Stay_Page_Click_Video', wait_time=5)
    def test_01(self):
        self.uihelper.click_element(MainPage['视频播放返回'])


if __name__ == '__main__':
    unittest.main(verbosity=2)
