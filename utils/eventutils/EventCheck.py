# -*- coding: UTF-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import os
from os import path
basedir = path.realpath(path.join(path.dirname(__file__), '../..'))
import utils.eventutils.EventHttpHandler as event_handler
from utils.eventutils.EventHttpHandler import *
from utils.eventutils.Verifier import Verifier
from utils import CommFun
import json
import unittest
import re
import requests
import time


app_map = {
        'toutiao':13,
        'studywell':1207,
    }


device_map = {
    '9995c69c45c1e73041fd1199a47bf5406962269c':51909725573
}

def check_step(*dargs, **dkargs):
    """
    :param dargs:
    :param dkargs: event wait_time
    :return: decoration
    """
    def wrapper(func):
        def _wrapper(*args, **kwargs):
            Logger.INFO("<-----Start Executing Event Case---->")
            if 'tea_event' in dkargs and dkargs['tea_event'] != 0:
                Logger.INFO(dkargs['tea_event'])
                start_time = int(time.time())
                CommFun.sleep(2)
                Logger.INFO("<-----Run Action---->")
                try:
                    func(*args, **kwargs)
                except BaseException as e:
                    Logger.ERROR("<-----Aciton Failed for {}---->".format(e.message))
                    import traceback
                    Logger.ERROR(traceback.print_exc())
                    CommFun.sleep(2)
                    raise unittest.TestCase.failureException
                wait_for_event(dkargs['wait_time'])
                end_time = int(time.time())
                aid = app_map[os.getenv('PRODUCT')]
                device_tt_id = os.getenv('DEVICETTID')
                dump_data = dump_tea_data(app_id=aid, start_time=start_time, end_time=end_time,device_id=device_tt_id,
                                          events=dkargs['tea_event'])
                received_events = analysis_tea_data(dump_data)
                # expect = get_expect_event(dkargs['tea_event'])['expect_event']
                expect = get_expect_event(dkargs['event'])['expect_event']
                Logger.INFO('-----start verify-----')
                result = verify_event_tea_data(expect, received_events)
            else:
                server_flag = os.getenv('EVENTSERVER')
                Logger.INFO(server_flag)
                if not server_flag:
                    EventHttpServer.PORT = 10303
                    local_ip = CommFun.get_local_ip()
                    EventHttpServer.start_server(local_ip)
                else:
                    print(u'已有服务启动')
                clear_received_event()
                start_observer()
                Logger.INFO("<-----Run Action---->")
                try:
                    func(*args, **kwargs)
                except:
                    Logger.ERROR("<-----Aciton Failed---->")
                    CommFun.sleep(2)
                    stop_observer()
                    raise unittest.TestCase.failureException
                wait_for_event(dkargs['wait_time'])
                stop_observer()
                received_events = get_received_events()
                expect = get_expect_event(dkargs['event'])['expect_event']
                Logger.INFO('-----start verify-----')
                result = verify_event_multi_without_count(expect, received_events)
            Logger.INFO('-----end verify-----')
            if not result['success']:
                Logger.ERROR(result)
                CommFun.sleep(2)
                if dkargs['event'] == 'Prepare':
                    Logger.INFO('PrepareCase')
                else:
                    raise unittest.TestCase.failureException
            else:
                print(result['success'])
        return _wrapper
    return wrapper


def start_server():
    print '----------------start_server----------------------'
    server = EventHttpServer(('0.0.0.0', EventHttpServer.PORT), EventHttpHandler)
    ip, port = server.server_address
    server_thread = threading.Thread(target=server.serve_forever)
    server_thread.setDaemon(True)
    server_thread.start()


def start_observer():
    print '---------------','start_observer'
    EventHttpHandler.start_observer()


def stop_observer():
    print '---------------', 'stop_observer'
    EventHttpHandler.stop_observer()


def wait_for_event(wait_time):
    CommFun.sleep(wait_time)


def get_received_events():
    return EventHttpHandler.received_events


def clear_received_event():
    Logger.INFO('-----clean events------')
    EventHttpHandler.clear_events()


def get_expect_event(events):
    """for multi event ,returns a list; for single event ,returns a dict"""
    local_path = path.abspath('.')
    if 'testcases' in local_path:
        filepath = re.findall(r'(.*)testcases', local_path, re.M)[0]
    else:
        evn_product = os.getenv('PRODUCT')
        filepath = local_path+'/product/'+evn_product
    return json.loads(open(filepath+'/jsonschema/'+events+'.json', 'r').read())

# def get_expect_event_multi():
#     # import pdb;pdb.set_trace()
#     return events_list.get_expect_event_list()[INFO]


def verify_event(expect):
    print 'test', 'verify_event'
    received_events = get_received_events()
    print '----------'
    print received_events
    print '----------'
    return Verifier().verify_by_jsonschema(expect, received_events)


def verify_event_multi_without_count(expect, received_events):
    # received_events = get_received_events()
    # print '----------'
    # print received_events
    # print '----------'
    return Verifier().verify_multi_event_without_count(expect, received_events)


def verify_event_tea_data(expect, received_events):
    return Verifier().verify_tea_event_multi(expect, received_events)


def dump_tea_data(app_id=13, device_id=51909725573, start_time=1528367120, end_time=1528367190, events=''):
    url = 'https://tea.bytedance.net/tea/openapi/behaviors/get-odin/'
    payload = {
        'caller': 'toutiao',  # 填写自己服务的psm.
        'device_id': device_id,  # 如果根据device_id查则参数换为device_id
    }
    Logger.INFO('tea odin request params: ' + str(payload))
    odin = requests.post(url, json.dumps(payload))
    odin = json.loads(odin.content).get('data')
    user_unique_id = u'{}_{}'.format(odin['user_type'],
                                     odin['user_id'])  # Note：有的产品不是odin体系，有自己的user_unique_id，则省去上面的代码逻辑。

    # get user behavior data.
    url = 'https://tea.bytedance.net/tea/openapi/behaviors/get-details/'
    payload = {
        'caller': 'toutiao',  # 填写自己服务的psm.
        'timestamp_from': start_time,
        'timestamp_to': end_time,
        'app_id': app_id,  # news_article=13
        'user_unique_id': user_unique_id,
        'events': events,
        # 'format': 'json'
    }
    Logger.INFO('tea request params: '+str(payload))
    res = requests.post(url, json.dumps(payload)).content
    data = json.loads(res)['data']
    Logger.INFO('tea res : ' + str(data))
    return data


def analysis_tea_data(data):
    tea_events = []
    for event in data:
        tea_events.append(event)
        Logger.INFO(event)
    return tea_events
# def verify_event_multi():
#     print 'test', 'verify_event'
#     received_events = get_received_events()
#     print '----------'
#     print received_events
#     print get_expect_event_multi()
#     print '----------'
#     return Verifier().verify_multievent(get_expect_event_multi(), received_events)


if __name__ == '__main__':
    os.environ["PRODUCT"] = 'toutiao'
    a = dump_tea_data(device_id=51909725573,start_time=1529579302,end_time=1529579378,events='stay_category')
    b = analysis_tea_data(a)
    expect = get_expect_event('Event_Huoshan_stay_page')['expect_event']
    result = verify_event_tea_data(expect,b)
    print result
