# -*- coding: UTF-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
# __author__ = 'guohao'
import requests
import urllib
import os
import importlib
import json
from time import sleep
# from urllib.parse import quote


class IOSUrlService(object):
    """
    Usage:
        Provide a method to jump to iOS App specified page via url scheme
        such as jump to iOS App shop activity witch shopid=xxx
    """

    def __init__(self):
        """
        """
        from utils import Logger
        self.log = Logger

    def jump_to_page(self, url):
        """
        jump to a page via url scheme
        :param url: url scheme
        :return: the respond of http requests
        """
        full_url = self.get_full_url()
        self.log.INFO(full_url)
        resp = None
        headers = {'content-type': 'application/json'}
        payload = {
            "schema": url
        }

        try:
            resp = requests.post(headers=headers,data=json.dumps(payload),url=full_url)
        except Exception as e:
            self.log.ERROR('Entry url failed: ' + full_url + ' schema: ' + url)
            self.log.ERROR('Error: '+str(e))
            return False
        finally:
            if resp:
                return True
            else:
                return False

    def get_full_url(self):
        """
        Usage:
            convert a url scheme to http requests url
        :param url: url scheme
        :return:  the full path of requests
        """
        port_map = {
            'com.ss.iphone.InHouse.article.News': "80"
        }
        port = port_map[os.getenv('BUNDLEID')]
        ip = os.getenv('DEVICEIP')
        return 'http://{}:{}/callout'.format(ip, port)


if __name__ == '__main__':
    print 'test'