# -*- coding: UTF-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

#!!!
MainPage = {
    #test
    'KEYCODE_HOME':'KEYCODE_HOME',
#启动处理
    '启动系统通知': '(//*[contains(@name,"发送通知")])',
    '启动允许':'允许',
    '领书领取':'立即领取',
    #首页
    '首页':  ('XCUIElementTypeApplication','CLASSNAME'),
    '频道tab':  '(textLabel)',
    '好书推荐One':'//XCUIElementTypeStaticText[@name="《拖延心理学》"]',
    '读好书':'读好书',
    # '读好书进入专辑':'(//XCUIElementTypeImage[@name="(imageView)"])[5]',
    '读好书进入专辑':'(//XCUIElementTypeCollectionView[@name="(collectionView)"])[3]/XCUIElementTypeCell[1]/XCUIElementTypeOther',
    '第二个频道':'(//XCUIElementTypeStaticText[@name="(textLabel)"])[2]',
    '首页tab':'(textLabel)',
    #免费音频更多
    '免费更多按钮': '(moreButton)',
    #每日免费
    # '免费列表点击': '(//XCUIElementTypeCell[@name="(Free)"])[1]',
    '免费列表点击':'(Free)',
    # '读好书':'//XCUIElementTypeApplication[@name="好好学习staging"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[4]/XCUIElementTypeOther[1]/XCUIElementTypeCollectionView/XCUIElementTypeCell[1]',
    '书籍详情页返回':'//XCUIElementTypeButton[@name="(leftItem)"]',
    '大咖好课':'(大咖好课)',
    '进入大咖好课':'(contentContainer)',
    # '进入大咖好课':'//XCUIElementTypeCollectionView[@name="(listView)"]/XCUIElementTypeCell',
    # '进入大咖好课':'//XCUIElementTypeCollectionView[@name="(listView)"]/XCUIElementTypeCell[8]/XCUIElementTypeOther',
    '专辑详情返回':'//XCUIElementTypeButton[@name="(leftItem)"]',
    '首页免费视频播放': '(playIconView)',
    '首页精彩挑战':'//XCUIElementTypeStaticText[@name="精彩挑战"]',
    '每日免费播放': '(playIconView)',
    '首页第一个挑战':'(//*[contains(@name,"随时加入")])',
    '每日免费播放一':'(//XCUIElementTypeImage[@name="(playIconView)"])[1]',
    '每日免费播放二':'(//XCUIElementTypeImage[@name="(playIconView)"])[2]',
    '每日免费播放三':'(//XCUIElementTypeImage[@name="(playIconView)"])[3]',
    '首页免费入口':'//XCUIElementTypeStaticText[@name="免费"]',
    #搜索相关埋点
    '首页搜索按钮':'//XCUIElementTypeButton[@name="(search_icon)"]',
    '搜索页输入框':'//XCUIElementTypeOther[@name="好好学习"]/XCUIElementTypeSearchField',
    '搜索按钮':'//XCUIElementTypeButton[@name="Search"]',
    '搜索内容':'1',
    '搜索结果一':'//XCUIElementTypeOther[@name="好好学习"]/XCUIElementTypeOther[6]',
    '搜索无结果推荐一':'//XCUIElementTypeStaticText[@name="《所谓情商高，就是会说话》"]',
    '搜索详情返回':'(button)',
    #icon二级页面
    '免费页面第一个':'//XCUIElementTypeApplication[@name="好好学习staging"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[1]/XCUIElementTypeOther',

    #挑战页面
    '第一个挑战':'//XCUIElementTypeApplication[@name="好好学习staging"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[1]/XCUIElementTypeOther[1]',
    '第二个挑战':'//XCUIElementTypeApplication[@name="好好学习staging"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[2]/XCUIElementTypeOther[1]',
    '第三个挑战':'//XCUIElementTypeApplication[@name="好好学习staging"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[3]/XCUIElementTypeOther[1]',
    '第四个挑战':'//XCUIElementTypeApplication[@name="好好学习staging"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[4]/XCUIElementTypeOther[1]',
    '第五个挑战':'//XCUIElementTypeApplication[@name="好好学习staging"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[5]/XCUIElementTypeOther[1]',
    '第六个挑战':'//XCUIElementTypeApplication[@name="好好学习staging"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[6]/XCUIElementTypeOther[1]',
    '付费参加挑战':'//XCUIElementTypeStaticText[@name="参与条件：付费即可参与"]',
    '免费参加挑战':'//XCUIElementTypeStaticText[@name="参与条件：免费"]',
    'VIP会员': '//XCUIElementTypeStaticText[@name="参与条件：VIP会员"]',
    '今日任务':'今日任务',
    '挑战tab返回': '(//XCUIElementTypeOther[@name=""])[1]',
    '参加收费挑战': '收费挑战￥29.90',
    '参加收费挑战去': '(//*[contains(@name,"收费挑战￥")])[1]',
    # '参加挑战': '(//XCUIElementTypeOther[@name="立即参加挑战"])[1]',
    '参加挑战': '立即参加挑战',
    '支付去充值':'去充值',
    '成为VIP': '成为VIP',
    '付费即可参与':'付费即可参与',
    # 导航栏
    '启动订阅': '订阅',
    '启动挑战': '挑战',
    '启动我的': '我的',
    '启动发现': '发现',
    #订阅栏目
    '点击订阅': '订阅',
    '点击下载': '下载',
    '第一订阅':  '//XCUIElementTypeApplication[@name="好好学习staging"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]',
    '订阅详情返回': '//XCUIElementTypeButton[@name="nav back white"]',
    '订阅book详情返回': 'nav back black',
    '订阅返回按钮':'//XCUIElementTypeButton[@name="(leftItem)"]',
    '订阅进入详情': '(//XCUIElementTypeImage[@name="download_arrow"])[1]',
    '点击我的': '我的',

    # 我的页面
    '我的':'我的',
    '积分':'积分',
    '学习':'学习(分钟)',
    '余额':'余额',
    '我的挑战':'我的挑战',
    '立即登录':'立即登录',
    '输入手机号码':'输入手机号码',
    '登录页面返回':'(view)',
    '获取验证码':'获取验证码',
    '输入验证码': '//XCUIElementTypeApplication[@name="好好学习staging"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther[2]/XCUIElementTypeOther',
    '线上账号': '用户4354993881',
    '测试账号': '用户9350103177',
    '开通VIP': '开通',
    #我的挑战页面
    '我的第一个挑战': '//XCUIElementTypeApplication[@name="好好学习staging"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell/XCUIElementTypeOther[1]',
    #VIP会员页
    '1个月会员':'(//XCUIElementTypeStaticText[@name="开通"])[3]',
    '6个月会员':'(//XCUIElementTypeStaticText[@name="开通"])[4]',
    '12个月会员':'(//XCUIElementTypeStaticText[@name="开通"])[5]',
    #大数据页面
    '大数据返回':'//XCUIElementTypeButton[@name="lefterbackicon titlebar"]',
    #更改埋点服务器
    '设置':'设置',
    '高级调试':'高级调试',
    '日志host': '(textField)',
    # '日志host':'//XCUIElementTypeTextField[@name="(textField)"]/XCUIElementTypeTextField',
    # '日志host':'//XCUIElementTypeApplication[@name="好好学习staging"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[8]/XCUIElementTypeOther[1]/XCUIElementTypeTextField',
    '配置gecko':'配置webview gecko channel',
    '地址输入':'//XCUIElementTypeAlert[@name="配置webview gecko channel"]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeTextField',
    'Done':'Done',
    '开启正式环境':'//XCUIElementTypeApplication[@name="好好学习staging"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[5]/XCUIElementTypeOther[1]/XCUIElementTypeSwitch',
    '调试返回':'lefterbackicon titlebar',
    '打开scheme':'打开一个URL Scheme',
    'scheme输入':'(//XCUIElementTypeTextField[@name="(textField)"])[2]',
    'scheme确定':'(//XCUIElementTypeButton[@name="(actionContentView)"])[2]',
    #音频bar
    '音频bar':'(//XCUIElementTypeOther[@name="(backgroundView)"])[1]',
    '音频bar切换':'(nextButton)',
    '音频bar关闭':'(closeContainer)',
    '音频bar播放暂停':'(playButton)',
    #系统控件
    '状态栏': '//XCUIElementTypeStatusBar[@name="(statusBar)"]',
    '状态栏倒回' : '倒回',
    '通知栏倒回' : '//XCUIElementTypeButton[@name="倒回"]',
    '通知栏暂停':'//XCUIElementTypeButton[@name="暂停"]',
    '状态栏暂停' : '暂停',
    '通知栏快进' : 'peButton[@name="快进"]',
    '状态栏快进' : '快进',
    #音频详情页
    '下一首': '(nextButton)',
    '更多button':'(moreButton)',
    '查看文稿':'查看文稿',
    #未够专辑详情页
    '播放按钮': '(//XCUIElementTypeImage[@name="icon"])',
    '购买按钮': '(priceButton)',
    '试听按钮': '(auditionButton)',
    '开会员按钮': '(becomeVipButton)',
    '详情分享': '(rightItem)',
    '分享微信好友':'//XCUIElementTypeApplication[@name="好好学习staging"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeButton[1]',
    '分享微信朋友圈':'//XCUIElementTypeApplication[@name="好好学习staging"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeButton[2]',
    # '确认支付': '//XCUIElementTypeStaticText[@name="确认支付"]',
    '确认支付': '确认支付',
    #已购专辑详情也
    '看图文tab': '(看图文)',
    '听音频tab': '(听音频)',
    '看资料tab': '(看资料)',
    '图文cell': '(ArticleCell)',
    '倒序': '(sortButton)',
    '音频tab播放全部': '(playAllButton)',
    '音频tab下载': '(downloadButton)',
    '音频tab下cell':'(AudioCell)',
    '图文详情播放':'//XCUIElementTypeOther[@name="好好学习"]/XCUIElementTypeOther[3]',
    '图文详情页面返回':'(leftItem)',
    '课程详情页面返回':'(leftItem)',
    '专辑文稿页点赞':  '(diggButtonWrapper)',
    #书籍详情页
    '听音频': '听音频',
    #系统设置页面
    '退出登录':'退出登录',
    '确定退出登录':'(rightButton)',
    # 视频详情页面
    '视频播放区域': '(controlsUnderlayView)',
    '视频详情返回': '(backButton)',
    '重播按钮':'(重播)',
    #频道
    '第一本书': '//XCUIElementTypeCollectionView[@name="(listView)"]/XCUIElementTypeCell[1]/XCUIElementTypeOther',
    '第一个专辑':'//XCUIElementTypeCollectionView[@name="(listView)"]/XCUIElementTypeCell'
}

