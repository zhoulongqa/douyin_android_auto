# -*- coding: utf-8 -*-
from product.toutiao.base.BaseTest import BaseTest
import unittest
from utils.eventutils import EventCheck
from product.toutiao.pages.MainPage import MainPage


class Event_Video_Play_Click_Video_Feed(BaseTest):
    def setUp(self):
        """
        埋点事件前置操作写在这里
        :return:
        """
        self.uihelper.click_element(MainPage['西瓜视频'])

    def tearDown(self):
        """
        恢复初始状态操作写在这里，如果没有多个Case，不需要些
        :return:
        """

    @EventCheck.check_step(event='Event_Video_Play_Click_Video_Feed', wait_time=5)
    def test_01(self):
        self.uihelper.sleep(5)
        self.uihelper.click_element(MainPage['列表播放视频'], index=1)


if __name__ == '__main__':
    unittest.main(verbosity=2)
