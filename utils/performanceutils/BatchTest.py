# -*- coding: utf-8 -*-
from os import path
import sys
basedir = path.realpath(path.join(path.dirname(__file__), '../..'))
sys.path.append(basedir)
reload(sys)
import multiprocessing
import subprocess
import time
import os
import re

competitor_list = [
    'com.baidu.BaiduMobile',
    'com.tencent.info',
    'com.tencent.reading',
    'com.baidu.BaiduMobileInfo',
    'com.sina.weibo',
]

app_url_list = [
    'http://tosv.byted.org/obj/iOSPackageBackUp/job/TT_iOS_learning_InHouse/lastSuccessfullBuild/Lemon.ipa',
    'http://tosv.byted.org/obj/iOSPackageBackUp/job/TT_iOS_AutoMobile_InHouse/lastSuccessfullBuild/AutoMobileInHouse.ipa',
    'http://tosv.byted.org/obj/iOSPackageBackUp/job/TT_iOS_EyeU_InHouse/lastSuccessfullBuild/EyeUInHouse.ipa',
    'http://tosv.byted.org/obj/iOSPackageBackUp/job/iOS_TTVideo_Player_InHouse/lastSuccessfullBuild/VideoInHouse.ipa',
    # 'http://tosv.byted.org/obj/iOSPackageBackUp/job/TT_iOS_Wenda_Inhouse/lastSuccessfullBuild/wendaInHouse.ipa',
    'http://tosv.byted.org/obj/iOSPackageBackUp/job/TT_iOS_StockInHouse_InHouse/lastSuccessfullBuild/StockInHouse.ipa',
    'http://tosv.byted.org/obj/iOSPackageBackUp/job/aweme_iOS_inhouse_test/lastSuccessfullBuild/AwemeInhouse.app.ipa',
    'http://tosv.byted.org/obj/iOSPackageBackUp/job/TT_iOS_AiKan_InHouse/lastSuccessfullBuild/NewsLiteInHouse.ipa',
]


def batch_competitor_test(run_times=1, member='guohao.qa'):
    for test in xrange(run_times):
        for i in competitor_list:
            os.system('curl -X POST "https://ci.bytedance.net/job/TT_iOS_LaunchTime_Test/'
                      'buildWithParameters?APP_URL=' + i + '&MEMBER=' + member + '&TASK='+str(test)+i+'"')


def batch_ipa_test(run_times=1, ipa_url='', member='guohao.qa'):
    for test in xrange(run_times):
        os.system('curl -X POST "https://ci.bytedance.net/job/TT_iOS_LaunchTime_Test/'
                  'buildWithParameters?APP_URL=' + ipa_url + '&MEMBER=' + member + '&TASK='+str(test)+'"')


def batch_inhouse_test(run_times=1, member='guohao.qa'):
    for test in xrange(run_times):
        for i in app_url_list:
            os.system('curl -X POST "https://ci.bytedance.net/job/TT_iOS_LaunchTime_Test/'
                      'buildWithParameters?APP_URL=' + i + '&MEMBER=' + member + '&TASK=' + str(test) +
                      str(app_url_list.index(i)) + '"')


if __name__ == "__main__":
    import sys
    from optparse import OptionParser

    parser = OptionParser()
    parser.add_option("-u", "--URL",
                      help="ipa url")
    parser.add_option("-d", "--device",
                      help="device udid")
    parser.add_option("-m", "--member",
                      help="member id")
    parser.add_option("-t", "--times",
                      help="run times")
    parser.add_option("-c", "--competitor",
                      help="competitor test")
    options, args = parser.parse_args(sys.argv)
    url = options.URL
    device = options.device
    member = options.member
    times = options.times
    competitor = options.competitor
    if competitor == '1':
        batch_competitor_test(int(times), member)
    elif competitor == '2':
        batch_inhouse_test(int(times), member)
    batch_ipa_test(int(times), url, member)