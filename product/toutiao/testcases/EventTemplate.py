# -*- coding: utf-8 -*-
from product.toutiao.base.BaseTest import BaseTest
import unittest
from utils.eventutils import EventCheck
from product.toutiao.pages.MainPage import MainPage
from utils import YAMLHelper
from utils.CommFun import Logger
from product.toutiao.testcases.commonprocess import PublicProcess
import os
import re


local_path = os.path.abspath('.')
if 'testcases' in local_path:
    filepath = re.findall(r'(.*)testcases', local_path, re.M)[0]
else:
    evn_product = os.getenv('PRODUCT')
    filepath = local_path + '/product/' + evn_product

case = YAMLHelper.open_YAML(filepath+'/testcases/yamlcase/Event_Detail_Go_Detail_Click_Video.yml')
if 'TeaEvent' in case:
    tea_event = case['TeaEvent']
else:
    tea_event = 0
if 'Schema' in case:
    event_schema = case['Schema']
else:
    event_schema = 0


class EventDemo(BaseTest):
    def setUp(self):
        actions = case['SetUp']
        if actions is not None:
            for action in actions:
                YAMLHelper.run_actions(self, action, MainPage)
        self.uihelper.sleep(1)

    @EventCheck.check_step(event=event_schema, wait_time=case['WaitTime'], tea_event=tea_event)
    def test_01(self):
        actions = case['Event']
        if actions is not None:
            for action in actions:
                YAMLHelper.run_actions(self, action, MainPage)
        if tea_event != 0:
            self.uihelper.sleep(5)
            self.uihelper.close_test_app()
            self.uihelper.launch_test_app()
        else:
            Logger.INFO('local data test')

    def tearDown(self):
        pass


if __name__ == '__main__':
    unittest.main(verbosity=2)
