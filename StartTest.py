# -*- coding: utf-8 -*-
import os
import unittest
from utils.HTMLTestRunner import HTMLTestRunner
from product.toutiao.testcases import *
from product.toutiao.testcases.basic import *
from product.toutiao.testcases.video import *
from product.toutiao.testcases.wenda import *
from product.toutiao.testcases.keypath import *
from casetemp import *
from utils.AppiumHelper import AppiumHelper
from utils import CommFun
from utils import Logger
from utils import CrashCatcher
from utils import DeviceHelper
import multiprocessing

all_cases = [
    KeyPathLogin.KeyPathLogin,
    KeyPathSearch.KeyPathSearch,
    KeyPathPostsDetail.KeyPathPostsDetail,
    KeyPathAnswerDetail.KeyPathAnswerDetail,
    KeyPathShortVideoDetail.KeyPathShortVideoDetail,
    KeyPathVideoDetail.KeyPathVideoDetail
]

event_cases = [
    Prepare.Prepare,
    Event_Category_Cate.Event_Category_Cate,
    Event_Category_Enter_Click_Cate.Event_Category_Enter_Click_Cate,
    Event_Category_Enter_Flip.Event_Category_Enter_Flip,
    Event_Category_Last_Read_Cate.Event_Category_Last_Read_Cate,
    Event_Category_Load_More_Cate.Event_Category_Load_More_Cate,
    Event_Category_Tab_No_Tip.Event_Category_Tab_No_Tip,
    Event_Detail_Write_Button.Event_Detail_Write_Button,
    Event_Go_Detail_Click_Feed.Event_Go_Detail_Click_Feed,
    Event_Go_Detail_Click_Headline.Event_Go_Detail_Click_Headline,
    Event_Navbar_Enter_Home_Click.Event_Navbar_Enter_Home_Click,
    Event_Navbar_Enter_Video_Click.Event_Navbar_Enter_Video_Click,
    Event_New_Tab_Enter_Click_Feed.Event_New_Tab_Enter_Click_Feed,
    Event_New_Tab_Enter_Flip.Event_New_Tab_Enter_Flip,
    Event_New_Tab_Feed_Cate.Event_New_Tab_Feed_Cate,
    Event_New_Tab_Feed_Tab_No_Tip.Event_New_Tab_Feed_Tab_No_Tip,
    Event_New_Tab_Last_Read_Feed.Event_New_Tab_Last_Read_Feed,
    Event_New_Tab_Load_More_Feed.Event_New_Tab_Load_More_Feed,
    Event_Refresh_Pull_Category.Event_Refresh_Pull_Category,
    Event_Refresh_Pull_Feed.Event_Refresh_Pull_Feed,
    Event_Stay_Category_All.Event_Stay_Category_All,
    Event_Stay_Page_Click_Headline.Event_Stay_Page_Click_Headline,
    Event_Xiangping_Write_Confirm.Event_Xiangping_Write_Confirm,
    Event_Go_Detail_Click_Video.Event_Go_Detail_Click_Video,
    Event_Stay_Page_Click_Video.Event_Stay_Page_Click_Video,
    Event_Video_Detail_Play.Event_Video_Detail_Play,
    Event_Video_Feed_Play.Event_Video_Feed_Play,
    Event_Video_Over_Click_Video.Event_Video_Over_Click_Video,
    Event_Video_Play_Click_Video.Event_Video_Play_Click_Video,
    Event_Video_Play_Click_Video_Feed.Event_Video_Play_Click_Video_Feed,
    Event_WD_Detail_Page_Click_Next_Answer.Event_WD_Detail_Page_Click_Next_Answer,
    Event_WD_Enter_Detail_Page.Event_WD_Enter_Detail_Page,
    Event_WD_Enter_List_Page.Event_WD_Enter_List_Page,
    Event_WD_Leave_Detail_Page.Event_WD_Leave_Detail_Page,
    Event_WD_Leave_Detail_Page_pct.Event_WD_Leave_Detail_Page_pct,
    Event_WD_Leave_List_Page.Event_WD_Leave_List_Page,
]

key_path_cases = [
    Prepare.Prepare,
    KeyPathLogin.KeyPathLogin,
    KeyPathSearch.KeyPathSearch,
    KeyPathPostsDetail.KeyPathPostsDetail,
    KeyPathAnswerDetail.KeyPathAnswerDetail,
    KeyPathShortVideoDetail.KeyPathShortVideoDetail,
    KeyPathVideoDetail.KeyPathVideoDetail
]


def set_test_case(case_list):
    global all_cases
    if case_list == 'EventCases':
        all_cases = event_cases
    elif case_list == 'KeyPath':
        all_cases = key_path_cases
    else:
        all_cases = eval(case_list)


def select_test_case(case_list):
    test_suite = unittest.TestSuite()
    for case in case_list:
        test_suite.addTest(unittest.makeSuite(case))
    return test_suite


def main(app_url='', udid='',case='', task_id='', product='toutiao'):
    # 准备Case
    if case:
        set_test_case(case)
    test_suite = select_test_case(all_cases)
    report_fp = 'report/testReport.html'
    html_report = open(report_fp, 'wb')
    runner = HTMLTestRunner(html_report,
                            verbosity=2,
                            title='测试报告',
                            description='执行人admin')

    # 准备配置
    cap = AppiumHelper.get_appium_config(platform='iOS', product=product)
    os.environ['PRODUCT'] = product
    if udid:
        os.environ['DEVICEID'] = udid
        device = udid
        try:
            os.environ['DEVICEIP'] = DeviceHelper.devices_info[udid]['ip']
            os.environ['DEVICETTID'] = DeviceHelper.devices_info[udid]['did']
        except:
            Logger.ERROR('unregistered device')
    else:
        device = cap['udid']
    app_name = cap['appName']
    bundle_id = cap['bundleId']
    if task_id:
        os.environ['TASKID'] = task_id

    # App安装
    if app_url:
        os.system('curl -o '+app_name+'.app.ipa '+app_url)
        os.system('ideviceinstaller -u '+device+' -U '+bundle_id)
        os.system('ideviceinstaller -u '+device+' -i '+app_name+'.app.ipa')

    # Appium 进程启动 WDA进程启动 iProxy进程启动
    appium_port = CommFun.get_available_port()
    os.environ['APPIUMADDR'] = '127.0.0.1:{0}'.format(appium_port)
    wda_port = CommFun.get_available_port()
    os.environ['WDAURL'] = 'http://127.0.0.1:{0}'.format(wda_port)

    Logger.INFO(
        '-----------------------start wda server-----%s--------%s-------' % (wda_port, device))
    wda_process = multiprocessing.Process(target=AppiumHelper.start_wda_server, args=(wda_port, device))
    wda_process.daemon = True
    wda_process.start()

    Logger.INFO(
        '-----------------------start appium server-----%s--------%s-------' % (appium_port, device))
    appium_process = multiprocessing.Process(target=AppiumHelper.start_appium_server,
                                             args=(appium_port, device, 10000, wda_port))
    appium_process.daemon = True
    appium_process.start()
    CommFun.sleep(15)

    Logger.INFO(
        '-----------------------start iProxy server-----%s--------%s-------' % (wda_port, device))
    iproxy_process = multiprocessing.Process(target=AppiumHelper.start_iproxy_server, args=(wda_port, wda_port, device))
    iproxy_process.daemon = True
    iproxy_process.start()
    CommFun.sleep(2)
    before = CrashCatcher.get_crash_num(app_name, udid)
    Logger.INFO('-------crash num before test: '+before)
    # 开始测试
    runner.run(test_suite)
    after = CrashCatcher.get_crash_num(app_name, udid)
    Logger.INFO('-------crash num after test: ' + after)
    if after > before:
        Logger.ERROR('有Crash出现,请检测crashlog文件内.ips日志')
        try:
            CrashCatcher.create_all_bugs()
        except:
            Logger.ERROR('创建JIRA失败')
    CommFun.sleep(5)


if __name__ == '__main__':
    import sys
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option("-u", "--URL",
                      help="ipa url")
    parser.add_option("-d", "--device",
                      help="device udid")
    parser.add_option("-c", "--cases",
                      help="test cases")
    parser.add_option("-t", "--task",
                      help="task id")
    parser.add_option("-p", "--product",
                      help="product name")
    options, args = parser.parse_args(sys.argv)
    app_url = options.URL
    udid = options.device
    cases = options.cases
    task_id = options.task
    product = options.product
    # app_url = 'http://ci.byted.org/view/iOS/job/TT_iOS_News_InHouse_Beta/lastSuccessfulBuild/artifact/Article/build/Release-iphoneos/NewsInHouse.ipa'
    # udid = '1e2af32a1b26b183807a7c71aed3dc845d63a084'
    # cases = '[Event1,Event2,Event3]'
    # task_id = '1'
    main(app_url=app_url, udid=udid, case=cases, task_id=task_id, product=product)
