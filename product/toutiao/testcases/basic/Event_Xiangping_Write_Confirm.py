# -*- coding: utf-8 -*-
from product.toutiao.base.BaseTest import BaseTest
import unittest
from utils.eventutils import EventCheck
from product.toutiao.pages.MainPage import MainPage


class Event_Xiangping_Write_Confirm(BaseTest):
    def setUp(self):
        """
        埋点事件前置操作写在这里
        :return:
        """
        self.uihelper.click_element(MainPage['Feed流列表'],index=2)

    def tearDown(self):
        """
        恢复初始状态操作写在这里，如果没有多个Case，不需要写
        :return:
        """

    @EventCheck.check_step(event='Event_Xiangping_Write_Confirm', wait_time=5)
    def test_01(self):
        self.uihelper.click_element(MainPage['写评论'])
        self.uihelper.enter(MainPage['评论输入框'],'this content is hahaha')
        self.uihelper.click_element(MainPage['发布评论'])


if __name__ == '__main__':
    unittest.main(verbosity=2)
