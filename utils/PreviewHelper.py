# -*- coding: UTF-8 -*-

import requests
from utils import Logger

PREVIEW_TEST_CHANNEL_URL = "http://10.8.163.168:8900/stream/fakedata/top"

class PreviewHelper(object):
    def __init__(self):
        pass

    @staticmethod
    def preview_in_test_channel(id):
        """在测试频道预览id对应的内容"""
        try:
            headers = {}
            headers['Pragma'] = 'no-cache'
            headers['Origin'] = 'http'
            headers['Accept-Encoding'] = 'gzip, deflate'
            headers['Accept-Language'] = 'zh-CN,zh;q=0.8,en;q=0.6'
            headers['User-Agent'] = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'
            headers['Content-Type'] = 'application/x-www-form-urlencoded'
            headers['Accept'] = 'application/json, text/plain, */*'
            headers['Cache-Control'] = 'no-cache'
            headers['Referer'] = 'http'
            headers['Connection'] = 'keep-alive'
            payload = {'_id': id}

            r = requests.post(PREVIEW_TEST_CHANNEL_URL, data=payload, headers=headers)
            if r.status_code == 200:
                print r.text
                return True
            else:
                return False
        except BaseException as e:
            import traceback
            print traceback.print_exc()
            """in case server is not stable"""
            Logger.ERROR("Preview item in test channel fail, please check reason")
            return False



if __name__ == "__main__":
    preview_helper = PreviewHelper()
    print preview_helper.preview_in_test_channel('5aa656923e9b136d75ed6d04')
    """
    精彩小视频1.5图 ：5a2541213e9b13370f6186ba
    测试评论的文章： 59b2074e3e9b1327b4d42143
    带视频问答： 5a7af3e13e9b13742f90edb2
    评论并转发帖子： 5aa656923e9b136d75ed6d04
    
    """
