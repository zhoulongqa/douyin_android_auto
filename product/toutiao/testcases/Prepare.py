# -*- coding: utf-8 -*-
import unittest
from product.toutiao.base.BaseTest import BaseTest
from product.toutiao.testcases.commonprocess import PublicProcess
from utils.eventutils.EventHttpHandler import *
from product.toutiao.configs.AccountConfig import AccountConfig
import os


class Prepare(BaseTest):
    def setUp(self):
        """
        test
        :return:
        """

    def tearDown(self):
        """
        test2
        :return:
        """

    def test_prepare(self):
        # self.uihelper.popup_window_detect_and_click()
        try:
            self.driver.switch_to.alert.accept()
            CommFun.Logger.INFO('弹窗处理成功')
            CommFun.sleep(2)
            self.driver.switch_to.alert.accept()
            CommFun.Logger.INFO('弹窗处理成功')
        except:
            CommFun.Logger.INFO('无弹窗需要处理')
        CommFun.sleep(1)
        PublicProcess.login(self, phone=AccountConfig.account, password=AccountConfig.password)
        port = CommFun.get_available_port()
        ip = CommFun.get_local_ip()
        host = ip + ":" + str(port)
        os.environ['EVENTSERVER'] = host
        process_args = {"args": ["-TTTrackRemoteServerCacheKey", "http://" + host]}
        os.environ['PROCESSARGS'] = str(process_args).replace("'", "\"")
        # host = os.getenv('EVENTSERVER')
        # PublicProcess.change_ip(self, host=host)
        EventHttpServer.PORT = port
        EventHttpServer.start_server(ip)


if __name__ == "__main__":
    unittest.main(verbosity=2)