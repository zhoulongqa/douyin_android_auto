# -*- coding: UTF-8 -*-

import sys
from os import path
basedir = path.realpath(path.join(path.dirname(__file__), '../..'))
sys.path.append(basedir)
import requests
reload(sys)
from utils import Logger
from utils.HTMLTestRunner import get_global_session
from utils.stfutils.DeviceManager import get_device_info
import subprocess
from threading import Timer


class AdbStrategy(object):
    def __init__(self,udid):
        self.has_timer_kill = False
        self.udid = udid
        pass

    def adb_pull(self, path_to_file, target_file):
        pass

    def execute_adb_shell(self, cmd,timeout):
        pass

    def install_app(self, app_path, has_permission, timeout):
        pass

    def uninstall_app(self, package_name, timeout):
        pass

    def execute_local_shell(self, cmd, timeout=None):
        """
        执行本地shell
        :param cmd: 命令字符串
        :param timeout: 设置超时时间，默认没有,单位为秒
        :return: (未超时bool，stdoutput, erroutput)
        """
        Logger.INFO(cmd)
        cmd_timer = None
        stdoutput = None
        erroutput = None
        p = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, shell=True)
        # time.sleep(10)
        if timeout is not None:
            try:
                timeout = int(timeout)
            except:
                timeout = 20
            cmd_timer = Timer(timeout, self._kill_proc_by_timer, [p])
            cmd_timer.start()
        try:
            (stdoutput, erroutput) = p.communicate()
        except:
            print 'except'
        finally:
            if cmd_timer is not None:
                cmd_timer.cancel()
        print stdoutput
        print erroutput
        return (not self.has_timer_kill, stdoutput, erroutput)

    def _kill_proc_by_timer(self, proc):
        if proc is not None:
            proc.kill()
            self.has_timer_kill = True



class stfAdb(AdbStrategy):
    """adb helper class for stf"""
    def __init__(self,udid):
        super(stfAdb,self).__init__(udid)
        self.session = get_global_session()
        self.deviceinfo = get_device_info(self.udid)
        self.execute_result = None
        self.stdoutput = ""
        self.stderrput = ""
        pass

    def on_reslove(self,dev, result):
        print result.success
        print result.last_data
        print result.error
        self.stdoutput = result.data
        self.stderrput = result.error
        self.execute_result = result.success


    def execute_adb_shell(self, cmd,timeout):
        Logger.INFO("execute stf adb shell cmd: {}".format(cmd))
        if self.session:
            # self.session.send_shell_cmd(self.deviceinfo, cmd, timeout_sec=timeout)
            self.session.send_shell_cmd_batch([self.deviceinfo,], cmd, timeout_sec=timeout, on_resolve=self.on_reslove)
            Logger.INFO('---adb-shell-stdoutput---')
            Logger.INFO(self.stdoutput)
            Logger.INFO('------------------------')
            return (self.execute_result, self.stdoutput, self.stderrput)
        else:
            Logger.ERROR("session is None")
            return (None, None, None)

    def _save_file_db(self, name, content):
        Logger.INFO("start saving file {}".format(name))
        if not content:
            content = 'get nothing'
        with open(name, 'wb') as f:
            f.write(content)
            Logger.INFO("end save file {}".format(name))

    def adb_pull(self, path_to_file, target_file, timeout):
        # '/sdcard/Android/data/com.ss.android.article.news/cache/debugger/debugger.db'
        HOST = 'http://thub.byted.org'
        def on_monkeylog_resolve(result):
            Logger.INFO("<------enter on_monkeylog_resolve------->")
            url = HOST + result.body['href'] + '?download'
            r = requests.get(url)
            self._save_file_db(target_file, r.content)
            # self._save_file(self.monkey_log_file, r.content)
        def on_pullfile_false(result):
            Logger.INFO("<-------pull file on reject------>")
            Logger.INFO(result)

        if self.session:
            self.session.fsretrieve(self.deviceinfo, path_to_file,
                                                on_resolve=on_monkeylog_resolve,
                                    on_reject = on_pullfile_false)

    def install_app(self, app_path, has_permission, timeout):
        Logger.INFO("execute stf adb install with app_path: {}".format(app_path))
        # if self.session:
        #     self.session.install(self.udid, href, manifest)
        pass

    def uninstall_app(self,package_name, timeout):
        Logger.INFO("execute stf adb uninstall package {}".format(package_name))
        if self.session:
            self.session.uninstall(self.deviceinfo, package_name)
        pass


class localAdb(AdbStrategy):
    def __init__(self,udid):
        super(localAdb,self).__init__(udid)
        pass


    def execute_adb_shell(self, cmd, timeout):
        """
         local adb shell
         :param cmd: 命令字符串
         :param timeout: 设置超时时间，默认没有,单位为秒
         :return: (未超时bool，stdoutput, erroutput)
        """
        Logger.INFO(cmd)
        cmd_timer = None
        stdoutput = None
        erroutput = None
        p = subprocess.Popen("adb -s %s shell" % self.udid, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                             shell=True)
        if timeout is not None:
            try:
                timeout = int(timeout)
            except:
                timeout = 20
            cmd_timer = Timer(timeout, self._kill_proc_by_timer, [p])
            cmd_timer.start()
        try:
            (stdoutput, erroutput) = p.communicate(cmd + "\nexit\n")
        except:
            print 'except'
        finally:
            if cmd_timer is not None:
                cmd_timer.cancel()
        print stdoutput
        print erroutput
        return (not self.has_timer_kill, stdoutput, erroutput)
        pass

    def adb_pull(self, path_to_file, target_file, timeout):
        cmd = "adb pull {0} {1}".format(path_to_file, target_file)
        return self.execute_local_shell(cmd, timeout)

    def install_app(self, app_path, has_permission=False, timeout=None):
        print 'install in localAdb'
        if has_permission:
            cmd = "adb -s %s install -g %s"%(self.udid,app_path)
        else:
            cmd = "adb -s %s install -r %s" % (self.udid, app_path)
        temp = self.execute_local_shell(cmd,timeout)
        # 解决偶发的安装失败的问题，后期可以考虑加入重试机制
        if temp[1] is not None and temp[1].count("INSTALL_FAILED_UID_CHANGED") > 0:
            return self.execute_local_shell(cmd, 30)
        else:
            return temp


    def uninstall_app(self, package_name, timeout):
        return self.execute_local_shell("adb -s %s uninstall %s" % (self.udid, package_name), timeout=timeout)


if __name__ == "__main__":
    localadb = localAdb("84B7N16411001465")
    (output0,output1,output2) = localadb.execute_adb_shell("getprop ro.product.brand",timeout=30)
    print output0, output1, output2


