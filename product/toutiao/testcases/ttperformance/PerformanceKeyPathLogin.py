# -*- coding: utf-8 -*-
import unittest
from product.toutiao.base.BaseTest import BaseTest
from product.toutiao.testcases.commonprocess import PublicProcess
from product.toutiao.configs import AccountConfig
from utils.performanceutils import UploadCaseInfo


class PerformanceKeyPathLogin(BaseTest):
    def setUp(self):
        """
        前置操作写在这里
        """
        self.start = UploadCaseInfo.get_format_time()

    def tearDown(self):
        """
        恢复初始状态操作写在这里，如果没有多个Case，不需要写
        """
        self.end = UploadCaseInfo.get_format_time()
        UploadCaseInfo.upload_case_info(pid=2, cid=0, value_type=1,
                                        start_time=self.start, end_time=self.end)
        self.uihelper.sleep(1)

    def test_01(self):
        PublicProcess.login(self, phone=AccountConfig.account, password=AccountConfig.password)


if __name__ == "__main__":
    unittest.main(verbosity=2)