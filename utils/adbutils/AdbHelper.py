# -*- coding: UTF-8 -*-
import re
from utils.HTMLTestRunner import get_global_session
from AdbStrategy import stfAdb, localAdb
from utils import Logger
import subprocess
from threading import Timer

class AdbHelper(object):

    def __init__(self, udid, isStf=False):
        self.udid = udid
        self.has_timer_kill = False
        if get_global_session():
            isStf = True
        else:
            isStf = False
        if isStf == True:
            self.adbstrategy = stfAdb(self.udid)
        else:
            self.adbstrategy = localAdb(self.udid)
        pass

    def execute_adb_shell(self, cmd, timeout=30):
        return self.adbstrategy.execute_adb_shell(cmd, timeout)

    def pull_file(self, path_to_file, target_file, timeout=60):
        self.adbstrategy.adb_pull(path_to_file, target_file, timeout=timeout)

    def install_app(self, app_path, has_permission=False, timeout=30):
        self.adbstrategy.install_app(app_path, has_permission, timeout)

    def uninstall_app(self, package_name, timeout=30):
        self.adbstrategy.uninstall_app(package_name, timeout)

    def remove_sdkcard_apk(self):
        return self.execute_adb_shell("rm /sdcard/Download/*.apk")

    def start_page_by_schema(self, schema):
        return self.execute_adb_shell("am start -a android.intent.action.VIEW "
                                      "-d %s --activity-clear-task" % schema)

    def get_devices_ip_address(self):
        l = self.execute_adb_shell('ip addr | grep global')
        reg = re.compile('\d+\.\d+\.\d+\.\d+')
        return re.findall(reg, l[1])[0]

    def get_device_brand(self):
        cmd = "getprop ro.product.brand"
        # (execute_result, stdout, stderr) = self.adb_helper.adbstrategy.execute_adb_shell(cmd,timeout=30)
        (execute_result, stdout, stderr) = self.execute_adb_shell(cmd, timeout=30)
        Logger.INFO("brand type is: {}".format(stdout))
        # brand = stdout.split("\n")[0]
        brand = stdout.split("\n")
        print len(brand)
        if len(brand)<=2:
            brandname = brand[0]
        else:
            brandname = stdout.split("\r\n")[3]
        #.split("\r")[0]
        return brandname


if __name__ == '__main__':
    adb_helper = AdbHelper("ENU7N16109004533", isStf=False)
    print adb_helper.get_device_brand()
