# -*- coding: utf-8 -*-
from product.toutiao.base.BaseTest import BaseTest
import unittest
from utils.eventutils import EventCheck
from product.toutiao.pages.MainPage import MainPage


class Event_WD_Detail_Page_Click_Next_Answer(BaseTest):
    def setUp(self):
        """
        埋点事件前置操作写在这里
        :return:
        """
        self.uihelper.click_element(MainPage['问答'])
        self.uihelper.click_element(MainPage['问答列表'])
        self.uihelper.click_element(MainPage['问答详情进入'])

    def tearDown(self):
        """
        恢复初始状态操作写在这里，如果没有多个Case，不需要些
        :return:
        """

    @EventCheck.check_step(event='Event_WD_Detail_Page_Click_Next_Answer', wait_time=5)
    def test_01(self):
        self.uihelper.click_element(MainPage['下一个回答'])


if __name__ == '__main__':
    unittest.main(verbosity=2)
