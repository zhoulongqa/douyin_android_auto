# -*- coding: utf-8 -*-
import unittest
from product.toutiao.base.BaseTest import BaseTest
from product.toutiao.pages.MainPage import MainPage
from utils.performanceutils import UploadCaseInfo


class PerformanceVideoFeed(BaseTest):
    def setUp(self):
        """
        前置操作写在这里
        """
        self.start = UploadCaseInfo.get_format_time()

    def tearDown(self):
        """
        恢复初始状态操作写在这里，如果没有多个Case，不需要写
        """
        self.end = UploadCaseInfo.get_format_time()
        UploadCaseInfo.upload_case_info(pid=2, cid=2, value_type=1,
                                        start_time=self.start, end_time=self.end)
        self.uihelper.sleep(1)

    def test_01(self):
        self.uihelper.click_element(MainPage['西瓜视频'])
        for i in xrange(10):
            self.uihelper.swipe_up(2)
            self.uihelper.swipe_down(2)
            self.uihelper.sleep(1)


if __name__ == "__main__":
    unittest.main(verbosity=2)